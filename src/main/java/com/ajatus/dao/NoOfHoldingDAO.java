package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.NoOfHolding;

public interface NoOfHoldingDAO {
	
	public void addNoOfHolding(NoOfHolding noOfHolding);
	public void updateNoOfHolding(NoOfHolding noOfHolding);
	public void removeNoOfHolding(int id);
	public List<NoOfHolding> listNoOfHolding();
	public NoOfHolding listNoOfHoldingById(int id);
}
