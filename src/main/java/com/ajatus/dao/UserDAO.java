/**
 * 
 */
package com.ajatus.dao;

/**
 * @author Girish
 *
 */
import java.util.List;
import com.ajatus.model.User;

public interface UserDAO {

	public void addUser(User p);
	public void updateUser(User p);
	public List<User> listUsers();
	public User getUserById(int id);
	public void removeUser(int id);
}
