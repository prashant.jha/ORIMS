package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.Ulb;

public interface UlbDAO {
	
	public void addUlb(Ulb u);
	public void updateUlb(Ulb u);
	public List<Ulb> listUlbs();
	public Ulb getUlbById(int id);
	public void removeUlb(int id);
	public List<Ulb> listUlbsByDistrict(int district_id);
}
