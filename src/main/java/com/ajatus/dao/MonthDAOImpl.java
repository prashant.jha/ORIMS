package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.Month;

@Repository
public class MonthDAOImpl implements MonthDAO{

	private static final Logger logger = LoggerFactory.getLogger(MonthDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addMonth(Month m) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(m);
		logger.info("Month saved successfully, Month details="+m);
		
	}

	@Override
	public void updateMonth(Month m) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(m);
		logger.info("Month updated successfully, Month details="+m);
		
	}

	@Override
	public List<Month> listMonths() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Month> monthsList = session.createQuery("from Month").list();
		for(Month m : monthsList){
			logger.info("Month List::"+m);
		}
		return monthsList;
	}

	@Override
	public Month getMonthById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Month m = (Month) session.load(Month.class, new Integer(id));
		logger.info("Month loaded successfully, Month details="+m);
		return m;
	}

	@Override
	public void removeMonth(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Month m = (Month) session.load(Month.class, new Integer(id));
		if(null != m){
			session.delete(m);
		}
		logger.info("Month deleted successfully, Month details="+m);
		
	}

}
