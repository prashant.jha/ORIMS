package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;

@Repository
public class DistrictDAOImpl implements DistrictDAO {

	private static final Logger logger = LoggerFactory.getLogger(DistrictDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addDistrict(District d) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(d);
		logger.info("District saved successfully, District details="+d);
		
	}

	@Override
	public void updateDistrict(District d) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(d);
		logger.info("District updated successfully, District details="+d);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<District> listDistricts() {
		Session session = this.sessionFactory.getCurrentSession();
		List<District> districtsList = session.createQuery("from District").list();
		for(District d : districtsList){
			logger.info("District List::"+d);
		}
		return districtsList;
	}

	@Override
	public District getDistrictById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		District d = (District) session.load(District.class, new Integer(id));
		logger.info("District loaded successfully, District details="+d);
		return d;
	}

	@Override
	public void removeDistrict(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		District d = (District) session.load(District.class, new Integer(id));
		if(null != d){
			session.delete(d);
		}
		logger.info("District deleted successfully, District details="+d);
		
	}

}
