package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.HoldingTaxPast;

public interface HoldingTaxPastDAO {
	
	public void addHoldingTaxPast(HoldingTaxPast holdingTaxPast);
	public void updateHoldingTaxPast(HoldingTaxPast holdingTaxPast);
	public void removeHoldingTaxPast(int id);
	public List<HoldingTaxPast> listHoldingTaxPast();
	public HoldingTaxPast listHoldingTaxPastById(int id);

}
