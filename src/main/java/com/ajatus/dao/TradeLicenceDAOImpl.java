package com.ajatus.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.model.TradeLicenceCollectionModel;
import com.ajatus.model.TradeLicenceCollectionPastModel;
import com.ajatus.model.TradeLicenceDemandModel;


@Repository
public class TradeLicenceDAOImpl implements TradeLicenceDAO {

	private static final Logger logger = LoggerFactory.getLogger(TradeLicenceDAOImpl.class);
	
	@Autowired(required=true)
	private SessionFactory sessionFactory;
	
	
	@Override
	public void addTradeLicenceDemand(TradeLicenceDemandModel tradeLicenceDemandModel) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(tradeLicenceDemandModel);
		logger.info("TradeLicenceDemand saved successfully, Ulb details="+tradeLicenceDemandModel);
		
	}


	@Override
	public void addTradeLicenceDemand(TradeLicenceCollectionModel tradeLicenceCollectionModel) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(tradeLicenceCollectionModel);
		logger.info("TradeLicenceDemand saved successfully, Ulb details="+tradeLicenceCollectionModel);      
		
	}


	@Override
	public void addTradeLicenceCollectionPast(TradeLicenceCollectionPastModel tradeLicenceCollectionPastModel) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(tradeLicenceCollectionPastModel);
		logger.info("TradeLicenceDemand saved successfully, Ulb details="+tradeLicenceCollectionPastModel);    
		
	}


	@Override
	public void addNoTradeLicence(TradeLicence tLicence) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(tLicence);
		logger.info("TradeLicenceDemand saved successfully, Ulb details="+tLicence);    
		
	}


	@Override
	public void addTradeBigDefaulters(TradeBigDefaultersModel tradeBigDefaultersModel) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(tradeBigDefaultersModel);
		logger.info("TradeLicenceDemand saved successfully, Ulb details="+tradeBigDefaultersModel);   
	}

}
