package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.TaxCollectorModel;

@Repository
public class TaxCollectorDAOImpl implements TaxCollectorDAO{

	private static final Logger logger = LoggerFactory.getLogger(WardOfficeDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addTaxCollector(TaxCollectorModel tc) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(tc);
		logger.info("Tax Collector saved successfully, Tax Collector details="+tc);
		
	}

	@Override
	public void updateTaxCollector(TaxCollectorModel tc) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(tc);
		logger.info("Tax Collector updated successfully, Tax Collector details="+tc);
		
	}

	@Override
	public List<TaxCollectorModel> listTaxCollectors() {
		Session session = this.sessionFactory.getCurrentSession();
		List<TaxCollectorModel> taxCollectorsList = session.createQuery("from TaxCollectorModel").list(); 
		for(TaxCollectorModel tc : taxCollectorsList){
			logger.info("Tax Collector List::"+tc);
		}
		return taxCollectorsList;
	}

	@Override
	public TaxCollectorModel getTaxCollectorById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		TaxCollectorModel tc = (TaxCollectorModel) session.load(TaxCollectorModel.class, new Integer(id));
		logger.info("Tax Collector loaded successfully, Tax Collector details="+tc);
		return tc;
	}

	@Override
	public void removeTaxCollector(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		TaxCollectorModel tc = (TaxCollectorModel) session.load(TaxCollectorModel.class, new Integer(id));
		if(null != tc){
			session.delete(tc);
		}
		logger.info("Tax Collector deleted successfully, Tax Collector details="+tc);
		
	}

	@Override
	public List<TaxCollectorModel> listTaxCollectorsByWard(int ward_id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<TaxCollectorModel> taxCollectorsList = session.createQuery("FROM TaxCollector w where w.ward.id= :ward_id")
				.setParameter("ward_id", ward_id)
				.list();
		for(TaxCollectorModel tc : taxCollectorsList){
			logger.info("Tax Collector List::"+tc);
		}
		return taxCollectorsList;
	}
}
