package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ajatus.model.Ulb;
import com.ajatus.model.Zone;

@Repository  
public class ZoneDAOImpl implements ZoneDAO{

	private static final Logger logger = LoggerFactory.getLogger(ZoneDAOImpl.class);

	@Autowired(required=true)   
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addZone(Zone zone) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(zone);
		
	}

	@Override
	public void updateZone(Zone zone) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(zone);
	}

	@Override
	public List<Zone> listZones() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Zone> zList = session.createQuery("from Zone").list();
		
		return zList;
	}

	@Override
	public Zone getZoneById(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Zone zone = (Zone) session.load(Zone.class, new Integer(id));
		
		return zone;
	}

	@Override
	public void removeZone(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Ulb u = (Ulb) session.load(Ulb.class, new Integer(id));
		if(null != u){
			session.delete(u);
		}
	}

	@Override
	public List<Zone> listZonesByUlb(int ulb_id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Zone> zList = session.createQuery("FROM Zone U where U.ulb.id= :ulb_id")
				.setParameter("ulb_id", ulb_id)
				.list();
		 return zList;
	}
	
	
}
