package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.CollectionHoldingTax;

public interface CollectionHoldingTaxDAO {

	public void addCollectionHoldingTax(CollectionHoldingTax h);
	public void updateCollectionHoldingTax(CollectionHoldingTax h);
	public List<CollectionHoldingTax> listCollectionHoldingTaxes();
	public CollectionHoldingTax getCollectionHoldingTaxById(int id);
	public List<CollectionHoldingTax> listCollectionHoldingTaxesByWard(int ward_id);
	public void removeCollectionHoldingTax(int id);
}
