package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.MarketRentalCollection;
import com.ajatus.model.MarketRentalCollectionPast;
import com.ajatus.model.MarketRentalProperties;

public interface MarketRentalDAO {

	public void addMarketRentalCollection(MarketRentalCollection marketRentalCollection);
	public void updateMarketRentalCollection(MarketRentalCollection marketRentalCollection);
	public void removeMarketRentalCollection(int id);
	public List<MarketRentalCollection> listMarketRentalCollection();
	public MarketRentalCollection listMarketRentalCollectionById(int id);

	public void addMarketRentalCollectionPast(MarketRentalCollectionPast marketRentalCollectionPast);
	public void updateMarketRentalCollectionPast(MarketRentalCollectionPast marketRentalCollectionPast);
	public void removeMarketRentalCollectionPast(int id);
	public List<MarketRentalCollectionPast> listMarketRentalCollectionPast();
	public MarketRentalCollectionPast listMarketRentalCollectionPastById(int id);
	
	public void addMarketRentalProperties(MarketRentalProperties mrp);
	public void updateMarketRentalProperties(MarketRentalProperties mrp);
	public void removeMarketRentalProperties(int id);
	public List<MarketRentalProperties> listMarketRentalProperties();
	public MarketRentalProperties listMarketRentalPropertiesById(int id);
}
