package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.DemandWard;

public interface DemandWardDAO {

	public void addDemandWard(DemandWard dw);
	public void updateDemandWard(DemandWard dw);
	public List<DemandWard> listDemandWards();
	public DemandWard getDemandWardById(int id);
	public void removeDemandWard(int id);
	public Boolean validateWardFinancialYear(int ward,int financialYear);
}
