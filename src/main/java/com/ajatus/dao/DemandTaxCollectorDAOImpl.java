package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.DemandTaxCollector;
import com.ajatus.model.DemandWard;

@Repository
public class DemandTaxCollectorDAOImpl implements DemandTaxCollectorDAO{

	private static final Logger logger = LoggerFactory.getLogger(DemandWardDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addDemandTaxCollector(DemandTaxCollector dt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(dt);
		logger.info("Demand Tax Collector saved successfully, Demand Tax Collector details="+dt);
		
	}

	@Override
	public void updateTaxCollector(DemandTaxCollector dt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(dt);
		logger.info("Demand Tax Collector updated successfully, Demand Tax Collector details="+dt);
		
	}

	@Override
	public List<DemandTaxCollector> listDemandTaxCollectors() {
		Session session = this.sessionFactory.getCurrentSession();
		List<DemandTaxCollector> demandTaxCollectorsList = session.createQuery("from DemandTaxCollector").list();
		for(DemandTaxCollector dt : demandTaxCollectorsList){
			logger.info("Demand Tax Collector List::"+dt);
		}
		return demandTaxCollectorsList;
	}

	@Override
	public DemandTaxCollector getDemandTaxCollectorById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		DemandTaxCollector dt = (DemandTaxCollector) session.load(DemandTaxCollector.class, new Integer(id));
		logger.info("Demand Tax Collector loaded successfully, Demand Tax Collector details="+dt);
		return dt;
	}

	@Override
	public List<DemandTaxCollector> listDemandTaxCollectorsByDemandWard(DemandWard demandWard) {
		Session session = this.sessionFactory.getCurrentSession();
		List<DemandTaxCollector> demandTaxCollectorsList = session.createQuery("from DemandTaxCollector dt where dt.demand_ward_id = :demand_ward_id")
														   .setParameter("demand_ward_id", demandWard.getId())
														   .list();
		for(DemandTaxCollector dt : demandTaxCollectorsList){
			logger.info("Demand Tax Collector List::"+dt);
		}
		return demandTaxCollectorsList;
	}

	@Override
	public void removeDemandTaxCollector(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		DemandTaxCollector dt = (DemandTaxCollector) session.load(DemandTaxCollector.class, new Integer(id));
		if(null != dt){
			session.delete(dt);
		}
		logger.info("Demand Tax Collector deleted successfully, Demand Tax Collector details="+dt);
		
	}

}
