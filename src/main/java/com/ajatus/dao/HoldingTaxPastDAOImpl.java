package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ajatus.model.HoldingTaxPast;
@Repository
public class HoldingTaxPastDAOImpl implements HoldingTaxPastDAO {

private static final Logger logger = LoggerFactory.getLogger(HoldingTaxPastDAOImpl.class);
	
	@Autowired(required=true)    
	private SessionFactory sessionFactory;
	
	@Override
	public void addHoldingTaxPast(HoldingTaxPast holdingTaxPast) {
		
		Session session = this.sessionFactory.getCurrentSession();
		try {
			session.persist(holdingTaxPast);
		} catch (Exception e) { 
			e.printStackTrace();
		}
		logger.info("Successfully Saved "+holdingTaxPast);	
		
	}

	@Override
	public void updateHoldingTaxPast(HoldingTaxPast holdingTaxPast) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeHoldingTaxPast(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HoldingTaxPast> listHoldingTaxPast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HoldingTaxPast listHoldingTaxPastById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
