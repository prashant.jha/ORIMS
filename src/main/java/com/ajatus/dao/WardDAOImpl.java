package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.Ulb;
import com.ajatus.model.Ward;

@Repository
public class WardDAOImpl implements WardDAO {

	private static final Logger logger = LoggerFactory.getLogger(WardDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addWard(Ward w) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(w);
		logger.info("Ward saved successfully, Ward details="+w);
	}

	@Override
	public void updateWard(Ward w) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(w);
		logger.info("Ward saved successfully, Ward details="+w);
	}

	@Override
	public List<Ward> listWards() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Ward> wardsList = session.createQuery("from Ward").list();
		for(Ward w : wardsList){
			//logger.info("Ward List::"+w);
		}
		return wardsList;
	}

	@Override
	public Ward getWardById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Ward w = (Ward) session.load(Ward.class, new Integer(id));
		logger.info("Ward loaded successfully, Ward details="+w);
		return w;
	}

	@Override
	public void removeWard(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Ward w = (Ward) session.load(Ward.class, new Integer(id));
		if(null != w){
			session.delete(w);
		}
		logger.info("Ward deleted successfully, Ward details="+w);
	}

	@Override
	public List<Ward> listWardsByUlb(int ulb_id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<Ward> wardsList = session.createQuery("FROM Ward w where w.ulb.id= :ulb_id ")
				.setParameter("ulb_id", ulb_id)
				.list();
		for(Ward w : wardsList){
			logger.info("Ward List::"+w);
		}
		return wardsList;
	}
	

}
