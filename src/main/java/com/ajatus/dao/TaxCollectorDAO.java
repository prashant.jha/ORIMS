package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.TaxCollectorModel;

public interface TaxCollectorDAO {

	public void addTaxCollector(TaxCollectorModel tc);
	public void updateTaxCollector(TaxCollectorModel tc);
	public List<TaxCollectorModel> listTaxCollectors();
	public TaxCollectorModel getTaxCollectorById(int id);
	public void removeTaxCollector(int id);
	public List<TaxCollectorModel> listTaxCollectorsByWard(int ward_id);
}
