package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;

@Repository
public class FinancialYearDAOImpl implements FinancialYearDAO{

	private static final Logger logger = LoggerFactory.getLogger(FinancialYearDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addFinancialYear(FinancialYear f) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(f);
		logger.info("Financial year saved successfully, Financial year details="+f);
		
	}

	@Override
	public void updateFinancialYear(FinancialYear f) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(f);
		logger.info("Financial year updated successfully, Financial year details="+f);
		
	}

	@Override
	public List<FinancialYear> listFinancialYears() {
		Session session = this.sessionFactory.getCurrentSession();
		List<FinancialYear> financialYearsList = session.createQuery("from FinancialYear").list();
		for(FinancialYear f : financialYearsList){
			logger.info("Financial year List::"+f);
		}
		return financialYearsList;
	}

	@Override
	public FinancialYear getFinancialYearById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		FinancialYear f = (FinancialYear) session.load(District.class, new Integer(id));
		logger.info("Financial year loaded successfully, Financial year details="+f);
		return f;
	}

	@Override
	public void removeFinancialYear(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		FinancialYear f = (FinancialYear) session.load(FinancialYear.class, new Integer(id));
		if(null != f){
			session.delete(f);
		}
		
		logger.info("Financial year deleted successfully, Financial year details="+f);
		
	}

}
