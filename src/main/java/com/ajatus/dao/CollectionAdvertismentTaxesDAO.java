package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.CollectionAdvertismentTaxes;

public interface CollectionAdvertismentTaxesDAO {
	
	public void addCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat);
	public void updateCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat);
	public void removeCollectionAdvertismentTaxes(int id);
	public List<CollectionAdvertismentTaxes> listCollectionAdvertismentTaxes();
	public CollectionAdvertismentTaxes listCollectionAdvertismentTaxesById(int id);

}
