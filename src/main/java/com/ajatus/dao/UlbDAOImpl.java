package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.District;
import com.ajatus.model.Ulb;

@Repository
public class UlbDAOImpl implements UlbDAO{

	private static final Logger logger = LoggerFactory.getLogger(UlbDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addUlb(Ulb u) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(u);
		logger.info("Ulb saved successfully, Ulb details="+u);
	}

	@Override
	public void updateUlb(Ulb u) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(u);
		logger.info("Ulb saved successfully, Ulb details="+u);
	}

	@Override
	public List<Ulb> listUlbs() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Ulb> ulbsList = session.createQuery("from Ulb").list();
		for(Ulb u : ulbsList){
			logger.info("Ulb List::"+u);
		}
		return ulbsList;
	}

	@Override
	public Ulb getUlbById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		Ulb u = (Ulb) session.load(Ulb.class, new Integer(id));
		logger.info("Ulb loaded successfully, Ulb details="+u);
		return u;
	}

	@Override
	public void removeUlb(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Ulb u = (Ulb) session.load(Ulb.class, new Integer(id));
		if(null != u){
			session.delete(u);
		}
		logger.info("Ulb deleted successfully, Ulb details="+u);
		
	} 
	
	@Override
	public List<Ulb> listUlbsByDistrict(int district_id) {
		
		// TODO Auto-generated method stub
		Session session = this.sessionFactory.getCurrentSession();
		List<Ulb> ulbsList = session.createQuery("FROM Ulb U where U.district.id= :district_id")
				.setParameter("district_id", district_id)
				.list();
		for(Ulb u : ulbsList){
			logger.info("Ulb List::"+u);
		}
		return ulbsList;
	}
}
