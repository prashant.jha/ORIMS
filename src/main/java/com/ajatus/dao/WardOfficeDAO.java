package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.WardOffice;

public interface WardOfficeDAO {

	public void addWardOffice(WardOffice wo);
	public void updateWardOffice(WardOffice wo);
	public List<WardOffice> listWardOffices();
	public WardOffice getWardOfficeById(int id);
	public void removeWardOffice(int id);
	public List<WardOffice> listWardOfficesByWard(int ward_id);
	
}
