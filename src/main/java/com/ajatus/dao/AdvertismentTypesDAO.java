package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.AdvertismentTypes;

public interface AdvertismentTypesDAO {
	
	public void addAdvertismentType(AdvertismentTypes at);
	public void updateAdvertismentType(AdvertismentTypes at);
	public void removeAdvertismentType(int id);
	public List<AdvertismentTypes> listAdvertismentType();
	public AdvertismentTypes listAdvertismentTypeById(int id);
	
}
