package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.AdvertismentTypes;
@Repository
public class AdvertismentTypesDAOImpl implements AdvertismentTypesDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(AdvertismentTypesDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addAdvertismentType(AdvertismentTypes at){
		
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(at);
		logger.info("Advertisment Type saved successfully, Advertisment Type details="+at);
		
	}
	
	@Override
	public void updateAdvertismentType(AdvertismentTypes at){
		
		Session session = this.sessionFactory.getCurrentSession();
		session.update(at);
		logger.info("Advertisment Type updated successfully, Advertisment Type details="+at);
	}
	
	@Override
	public void removeAdvertismentType(int id){
		
		Session session = this.sessionFactory.getCurrentSession();
		AdvertismentTypes at = (AdvertismentTypes) session.load(AdvertismentTypes.class, new Integer(id));
		if(null != at){
			session.delete(at);
		}
		logger.info("Advetisment Type deleted successfully, Advetisment Types details="+at);
	}
	
	@Override
	public AdvertismentTypes listAdvertismentTypeById(int id){
		
		Session session = this.sessionFactory.getCurrentSession();
		AdvertismentTypes at = (AdvertismentTypes) session.load(AdvertismentTypes.class, new Integer(id));
		logger.info("Advetisment Type loaded successfully, Advetisment Types details="+at);
		return at;
	}
	
	@Override
	public List<AdvertismentTypes> listAdvertismentType(){
		
		Session session = this.sessionFactory.getCurrentSession();
		List<AdvertismentTypes> advertismentTypesList = session.createQuery("from AdvertismentTypes").list();
		for(AdvertismentTypes at : advertismentTypesList){
			logger.info("Advertisment Types List::"+at);
		}
		return advertismentTypesList;
	}


}
