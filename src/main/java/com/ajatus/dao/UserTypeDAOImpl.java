package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.UserType;


@Repository
public class UserTypeDAOImpl implements UserTypeDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(UserTypeDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addUserType(UserType p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(p);
		logger.info("UsesType saved successfully, UserType Details="+p);
	}

	@Override
	public void updateUserType(UserType p) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(p);
		logger.info("UserType updated successfully, UserType Details="+p);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<UserType> listUserTypes() {
		Session session = this.sessionFactory.getCurrentSession();
		List<UserType> UsersTypesList = session.createQuery("from UserType").list();
		for(UserType p : UsersTypesList){
			logger.info("UserType List::"+p);
		}
		return UsersTypesList;
	}

	@Override
	public UserType getUserTypeById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		UserType p = (UserType) session.load(UserType.class, new Integer(id));
		logger.info("UserType loaded successfully, UserType details="+p);
		return p;
	}

	@Override
	public void removeUserType(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		UserType p = (UserType) session.load(UserType.class, new Integer(id));
		if(null != p){
			session.delete(p);
		}
		logger.info("UserType deleted successfully, UserType details="+p);
	}

	

}