package com.ajatus.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.DemandWard;
import com.ajatus.model.District;

@Repository
public class DemandWardDAOImpl implements DemandWardDAO{

	private static final Logger logger = LoggerFactory.getLogger(DemandWardDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addDemandWard(DemandWard dw) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(dw);
		logger.info("Demand Ward saved successfully, Demand Ward details="+dw);
	}

	@Override
	public void updateDemandWard(DemandWard dw) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(dw);
		logger.info("Demand Ward updated successfully, Demand Ward details="+dw);
		
	}

	@Override
	public List<DemandWard> listDemandWards() {
		Session session = this.sessionFactory.getCurrentSession();
		List<DemandWard> demandWardsList = session.createQuery("from DemandWard").list();
		for(DemandWard dw : demandWardsList){
			logger.info("Demand Ward List::"+dw);
		}
		return demandWardsList;
	}

	@Override
	public DemandWard getDemandWardById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		DemandWard dw = (DemandWard) session.load(DemandWard.class, new Integer(id));
		logger.info("Demand Ward loaded successfully, Demand Ward details="+dw);
		return dw;
	}

	@Override
	public void removeDemandWard(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		DemandWard dw = (DemandWard) session.load(DemandWard.class, new Integer(id));
		if(null != dw){
			session.delete(dw);
		}
		logger.info("Demand Ward deleted successfully, Demand Ward details="+dw);
		
	}
	@Override
	public Boolean validateWardFinancialYear(int ward_id,int financialYear_id){
		Session session = this.sessionFactory.getCurrentSession();
		Query query =session.createQuery("from DemandWard where ward_id=:ward_id and financial_year_id=:financialYear_id");
		query.setParameter("ward_id",ward_id);
		query.setParameter("financialYear_id", financialYear_id);
		return query.list().isEmpty();
	}

}
