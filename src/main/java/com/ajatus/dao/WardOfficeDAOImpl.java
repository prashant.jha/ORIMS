package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.Ward;
import com.ajatus.model.WardOffice;

@Repository
public class WardOfficeDAOImpl implements WardOfficeDAO {

	private static final Logger logger = LoggerFactory.getLogger(WardOfficeDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addWardOffice(WardOffice wo) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(wo);
		logger.info("Ward office saved successfully, Ward office details="+wo);
		
	}

	@Override
	public void updateWardOffice(WardOffice wo) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(wo);
		logger.info("Ward office updated successfully, Ward office details="+wo);
		
	}

	@Override
	public List<WardOffice> listWardOffices() {
		Session session = this.sessionFactory.getCurrentSession();
		List<WardOffice> wardOfficesList = session.createQuery("from WardOffice").list();
		for(WardOffice wo : wardOfficesList){
			logger.info("WardOffice List::"+wo);
		}
		return wardOfficesList;
	}

	@Override
	public WardOffice getWardOfficeById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		WardOffice wo = (WardOffice) session.load(WardOffice.class, new Integer(id));
		logger.info("Ward Office loaded successfully, Ward Office details="+wo);
		return wo;
	}

	@Override
	public void removeWardOffice(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		WardOffice wo = (WardOffice) session.load(WardOffice.class, new Integer(id));
		if(null != wo){
			session.delete(wo);
		}
		logger.info("Ward Office deleted successfully, Ward Office details="+wo);
		
	}

	@Override
	public List<WardOffice> listWardOfficesByWard(int ward_id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<WardOffice> wardOfficesList = session.createQuery("FROM WardOffice w where w.ward.id= :ward_id")
				.setParameter("ward_id", ward_id)
				.list();
		for(WardOffice wo : wardOfficesList){
			logger.info("Ward Office List::"+wo);
		}
		return wardOfficesList;
	}

}
