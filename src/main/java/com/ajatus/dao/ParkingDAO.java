package com.ajatus.dao;

import com.ajatus.model.AssetParkingLotAuctioned;
import com.ajatus.model.AssetParkingLotULB;
import com.ajatus.model.ParkingLots;
import com.ajatus.model.ParkingLotsPast;

public interface ParkingDAO {
	public void addParkingLots(ParkingLots parkinglots );

	public void addParkingLotsPast(ParkingLotsPast parkingLotsPast);

	public void addAssetParkingLotULB(AssetParkingLotULB asParkingLotULB);

	public void addAssetParkingLotAuctioned(AssetParkingLotAuctioned assetParkingLotAuctioned);
}
