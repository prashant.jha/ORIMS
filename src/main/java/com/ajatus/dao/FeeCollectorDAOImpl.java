package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ajatus.model.FeeCollector;

@Repository
public class FeeCollectorDAOImpl implements FeeCollectorDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(FeeCollectorDAOImpl.class);

	@Autowired(required=true)
	private SessionFactory sessionFactory;
	
	

	@Override
	public void addFeeCollectorDAO(FeeCollector d) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(d);
		
	}

	@Override
	public void updateFeeCollector(FeeCollector d) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(d);
		
	}
	
	
	@Override
	public List<FeeCollector> listFeeCollector() {
		Session session = this.sessionFactory.getCurrentSession();
		List<FeeCollector> feeCollectorList = session.createQuery("from FeeCollector").list();
		
		return feeCollectorList;
	}

	@Override
	public FeeCollector getFeeCollectorById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		FeeCollector d = (FeeCollector) session.load(FeeCollector.class, new Integer(id));
		logger.info("FeeCollectorModel loaded successfully, FeeCollectorModel details="+d);
		return d;
	}

	@Override
	public void removeFeeCollector(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		FeeCollector d = (FeeCollector) session.load(FeeCollector.class, new Integer(id));
		if(null != d){
			session.delete(d);
		}
		logger.info("FeeCollectorModel deleted successfully, FeeCollectorModel details="+d);
		
		
	}

}
