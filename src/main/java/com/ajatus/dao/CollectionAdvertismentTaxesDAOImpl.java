package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.CollectionAdvertismentTaxes;

@Repository
public class CollectionAdvertismentTaxesDAOImpl implements CollectionAdvertismentTaxesDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(CollectionAdvertismentTaxesDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat){
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(cat);
		logger.info("Collection Advertisment Tax saved successfully, Collection Advertisment Tax="+cat);
	}
	
	@Override
	public void updateCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat){
		Session session = this.sessionFactory.getCurrentSession();
		session.update(cat);
		logger.info("Collection Advertisment Tax updated successfully, Collection Advertisment Tax details="+cat);
	}
	
	@Override
	public void removeCollectionAdvertismentTaxes(int id){
		Session session = this.sessionFactory.getCurrentSession();
		CollectionAdvertismentTaxes cat = (CollectionAdvertismentTaxes) session.load(CollectionAdvertismentTaxes.class, new Integer(id));
		if(null != cat){
			session.delete(cat);
		}
		logger.info("Collection Advertisment Tax deleted successfully, Collection Advertisment Tax details="+cat);
	}
	
	@Override
	public CollectionAdvertismentTaxes listCollectionAdvertismentTaxesById(int id){
		
		Session session = this.sessionFactory.getCurrentSession();
		CollectionAdvertismentTaxes at = (CollectionAdvertismentTaxes) session.load(CollectionAdvertismentTaxes.class, new Integer(id));
		logger.info("Collection Advertisment Taxes loaded successfully, Collection Advertisment Taxes details="+at);
		return at;
	}
	@Override
	public List<CollectionAdvertismentTaxes> listCollectionAdvertismentTaxes(){
		Session session = this.sessionFactory.getCurrentSession();
		List<CollectionAdvertismentTaxes> collectionAdvertismentTaxesList = session.createQuery("from CollectionAdvertismentTaxes").list();
		for(CollectionAdvertismentTaxes cat : collectionAdvertismentTaxesList){
			logger.info("Collection Advertisment Taxes List::"+cat);
		}
		return collectionAdvertismentTaxesList;

	}

}
