package com.ajatus.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ajatus.model.AssetParkingLotAuctioned;
import com.ajatus.model.AssetParkingLotULB;
import com.ajatus.model.ParkingLots;
import com.ajatus.model.ParkingLotsPast;

@Repository
public class ParkingDAOImpl implements ParkingDAO {

	@Autowired(required=true)
    private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}

	@Override
	public void addParkingLots(ParkingLots parkinglots ) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(parkinglots);
		
	}

	@Override
	public void addParkingLotsPast(ParkingLotsPast parkingLotsPast) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(parkingLotsPast);
		
	}

	@Override
	public void addAssetParkingLotULB(AssetParkingLotULB asParkingLotULB) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(asParkingLotULB);
	}

	@Override
	public void addAssetParkingLotAuctioned(AssetParkingLotAuctioned assetParkingLotAuctioned) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(assetParkingLotAuctioned);
	}
}
