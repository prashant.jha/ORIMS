package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.ajatus.model.CollectionHoldingTax;

@Repository
public class CollectionHoldingTaxDAOImpl implements CollectionHoldingTaxDAO{

	private static final Logger logger = LoggerFactory.getLogger(CollectionHoldingTaxDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
	
	@Override
	public void addCollectionHoldingTax(CollectionHoldingTax h) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(h);
		logger.info("Collection Holding tax saved successfully, Holding tax details="+h);
	}

	@Override
	public void updateCollectionHoldingTax(CollectionHoldingTax h) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(h);
		logger.info("Collection Holding tax updated successfully, Holding tax details="+h);
		
	}

	@Override
	public List<CollectionHoldingTax> listCollectionHoldingTaxes() {
		Session session = this.sessionFactory.getCurrentSession();
		List<CollectionHoldingTax> CollectionHoldingTaxList = session.createQuery("from CollectionHoldingTax").list();
		for(CollectionHoldingTax h : CollectionHoldingTaxList){
			logger.info("Collection Holding tax List::"+h);
		}
		return CollectionHoldingTaxList;
	}

	@Override
	public CollectionHoldingTax getCollectionHoldingTaxById(int id) {
		Session session = this.sessionFactory.getCurrentSession();		
		CollectionHoldingTax h = (CollectionHoldingTax) session.load(CollectionHoldingTax.class, new Integer(id));
		logger.info("Collection Holding tax loaded successfully, Holding tax details="+h);
		return h;
	}

	@Override
	public void removeCollectionHoldingTax(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		CollectionHoldingTax h = (CollectionHoldingTax) session.load(CollectionHoldingTax.class, new Integer(id));
		if(null != h){
			session.delete(h);
		}
		logger.info("Collection Holding tax deleted successfully, Holding tax details="+h);
		
	}

	@Override
	public List<CollectionHoldingTax> listCollectionHoldingTaxesByWard(int ward_id) {
		Session session = this.sessionFactory.getCurrentSession();
		List<CollectionHoldingTax> CollectionHoldingTaxList = session.createQuery("from CollectionHoldingTax h where h.ward.id= :ward_id")
				.setParameter("ward_id", ward_id)
				.list();
		for(CollectionHoldingTax h : CollectionHoldingTaxList){
			logger.info("Collection Holding tax List::"+h);
		}
		return CollectionHoldingTaxList;
	}

}
