package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.District;

public interface DistrictDAO {

	public void addDistrict(District d);
	public void updateDistrict(District d);
	public List<District> listDistricts();
	public District getDistrictById(int id);
	public void removeDistrict(int id);
}
