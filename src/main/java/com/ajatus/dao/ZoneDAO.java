package com.ajatus.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ajatus.model.Ulb;
import com.ajatus.model.Zone;


public interface ZoneDAO {
	
	public void addZone(Zone u);
	public void updateZone(Zone u);
	public List<Zone> listZones();
	public Zone getZoneById(int id);
	public void removeZone(int id);
	public List<Zone> listZonesByUlb(int ulb_id);
}
