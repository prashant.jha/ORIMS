package com.ajatus.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ajatus.model.MarketRentalCollection;
import com.ajatus.model.MarketRentalCollectionPast;
import com.ajatus.model.MarketRentalProperties;

@Repository
public class MarketRentalDAOImpl implements MarketRentalDAO {


	
private static final Logger logger = LoggerFactory.getLogger(MarketRentalDAOImpl.class);

	@Autowired(required=true)    
	private SessionFactory sessionFactory;
	
	
	
	@Override
	public void addMarketRentalProperties(MarketRentalProperties mrp) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(mrp);
		logger.info("Successfully Saved "+mrp);
		
	}

	@Override
	public void updateMarketRentalProperties(MarketRentalProperties mrp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMarketRentalProperties(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MarketRentalProperties> listMarketRentalProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MarketRentalProperties listMarketRentalPropertiesById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	

	
	@Override
	public void addMarketRentalCollectionPast(MarketRentalCollectionPast marketRentalCollectionPast) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(marketRentalCollectionPast);
		logger.info("Successfully Saved "+marketRentalCollectionPast);
		
	}

	@Override
	public void updateMarketRentalCollectionPast(MarketRentalCollectionPast marketRentalCollectionPast) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMarketRentalCollectionPast(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MarketRentalCollectionPast> listMarketRentalCollectionPast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MarketRentalCollectionPast listMarketRentalCollectionPastById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addMarketRentalCollection(MarketRentalCollection marketRentalCollection) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(marketRentalCollection);
		logger.info("Successfully Saved "+marketRentalCollection);
		
	}

	@Override
	public void updateMarketRentalCollection(MarketRentalCollection marketRentalCollection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMarketRentalCollection(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MarketRentalCollection> listMarketRentalCollection() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MarketRentalCollection listMarketRentalCollectionById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
