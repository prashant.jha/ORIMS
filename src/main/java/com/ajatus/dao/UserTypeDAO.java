/**
 * 
 */
package com.ajatus.dao;

/**
 * @author Girish
 *
 */
import java.util.List;

import com.ajatus.model.UserType;

public interface UserTypeDAO {

	public void addUserType(UserType p);
	public void updateUserType(UserType p);
	public List<UserType> listUserTypes();
	public UserType getUserTypeById(int id);
	public void removeUserType(int id);
}
