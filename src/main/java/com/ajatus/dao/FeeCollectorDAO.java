package com.ajatus.dao;

import java.util.List;

import com.ajatus.model.District;
import com.ajatus.model.FeeCollector;

public interface FeeCollectorDAO {
	public void addFeeCollectorDAO(FeeCollector d);
	public void updateFeeCollector(FeeCollector d);
	public List<FeeCollector> listFeeCollector();
	public FeeCollector getFeeCollectorById(int id);
	public void removeFeeCollector(int id);
}
