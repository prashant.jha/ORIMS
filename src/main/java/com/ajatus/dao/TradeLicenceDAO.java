package com.ajatus.dao;

import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.model.TradeLicenceCollectionModel;
import com.ajatus.model.TradeLicenceCollectionPastModel;
import com.ajatus.model.TradeLicenceDemandModel;

public interface TradeLicenceDAO {

	void addTradeLicenceDemand(TradeLicenceDemandModel tradeLicenceDemandModel);

	void addTradeLicenceDemand(TradeLicenceCollectionModel tradeLicenceCollectionModel);

	void addTradeLicenceCollectionPast(TradeLicenceCollectionPastModel tradeLicenceCollectionPastModel);


	void addNoTradeLicence(TradeLicence tLicence);

	void addTradeBigDefaulters(TradeBigDefaultersModel tradeBigDefaultersModel);

}
