package com.ajatus.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;
import com.ajatus.model.Month;
import com.ajatus.model.Ulb;
import com.ajatus.model.Ward;

public class HoldingTaxDto {
	private FinancialYear financialYear;
	private District district;
	private Ulb ulb;
	private Ward ward;
	private Month month;
	private double currentCollection;
	private double arrear;
	@Min(
            value = 1,
            message = "This field should not have 0"
    )
	private int noOfHoldings;
	@Min(
            value = 1,
            message = "This field should not have 0"
    )
	private int commercialHoldings;
	@Min(
            value = 1,
            message = "This field should not have 0"
    )
	private int residentalHoldings;  
	
	public Ward getWard() {
		return ward;
	}
	public void setWard(Ward ward) {
		this.ward = ward;
	}
	public int getNoOfHoldings() {
		return noOfHoldings;
	}
	public void setNoOfHoldings(int noOfHoldings) {
		this.noOfHoldings = noOfHoldings;
	}
	public int getCommercialHoldings() {
		return commercialHoldings;
	}
	public void setCommercialHoldings(int commercialHoldings) {
		this.commercialHoldings = commercialHoldings;
	}
	public int getResidentalHoldings() {
		return residentalHoldings;
	}
	public void setResidentalHoldings(int residentalHoldings) {
		this.residentalHoldings = residentalHoldings;
	}	
	
	public FinancialYear getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public Ulb getUlb() {
		return ulb;
	}
	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}
	public Month getMonth() {
		return month;
	}
	public void setMonth(Month month) {
		this.month = month;
	}
	public double getCurrentCollection() {
		return currentCollection;
	}
	public void setCurrentCollection(double currentCollection) {
		this.currentCollection = currentCollection;
	}
	public double getArrear() {
		return arrear;
	}
	public void setArrear(double arrear) {
		this.arrear = arrear;
	}

}
