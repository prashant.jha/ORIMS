package com.ajatus.dto;

import java.util.Date;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;
import com.ajatus.model.Ulb;

public class MarketRental {
	private District district;
	private Ulb ulb;
	private String types;
	private int numbers;
	private FinancialYear financialYear;
	private double amount;
	private Date date;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public FinancialYear getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	
	
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public Ulb getUlb() {
		return ulb;
	}
	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}
	public String getTypes() {
		return types;
	}
	public void setTypes(String types) {
		this.types = types;
	}
	public int getNumbers() {
		return numbers;
	}
	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}
	
}
