package com.ajatus.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {
	private static Pattern pattern;
	private static Matcher matcher;

	public static boolean validateUid(String uid) {
		  pattern = Pattern.compile("\\d{3,12}");
		  matcher = pattern.matcher(uid);
		return matcher.matches();
	}
	public static boolean validateStartWithZero(String uid) {
		 
		return uid.startsWith("0");		
	}
	
	public static boolean validateAge(String age) {
		  pattern = Pattern.compile("\\d{1,3}");
		  matcher = pattern.matcher(age);
		return matcher.matches();
	}
	
	public static boolean validateOtp(String otp) {
		  pattern = Pattern.compile("\\d{6}");
		  matcher = pattern.matcher(otp);
		return matcher.matches();
	}

	public static boolean validateEid(String eid) {
		return eid.matches("\\d{3,28}");
	}

	public static boolean validateName(String name) {

		return name.matches("([a-zA-Z.'\\s]){3,60}");
	}

	public static boolean validateDesignation(String name) {

		return name.matches("([a-zA-Z.'\\s]){1,60}");
	}
	

	public static boolean validateEmail(String email) {

		return email
				.matches("^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
	}

	public static boolean validateStreet(String street) {

		return street.matches("([a-zA-Z0-9(:)#-;,.\\s\\/]){3,60}");
	}

	public static boolean validateBuilding(String building) {

		return building.matches("([a-zA-Z0-9(:)#-;,.\\s\\/]){3,60}");
	}

	public static boolean validateLocality(String locality) {

		return locality.matches("([a-zA-Z0-9(:)#-;,.\\s\\/]){3,60}");
	}

	public static boolean validateVtc(String vtc) {

		return vtc.matches("([a-zA-Z\\s]){6,50}");
	}

	public static boolean validateDistrict(String district) {

		return district.matches("([a-zA-Z\\s]){3,60}");
	}

	public static boolean validateState(String State) {

		return State.matches("([a-zA-Z\\s]){3,60}");
	}

	public static boolean validatePincode(String pincode) {

		return pincode.matches("\\d{3,6}");
	}

	public static boolean validateMobile(String mobile) {

		return mobile.matches("\\d{10,11}");
	}
	
	public static boolean validateFileType(String fileName) {

		return fileName.matches("(.*?)\\.(pdf|ppt)$");
	}
	public static boolean validateFileType1(String fileName) {

		return fileName.matches("(.*?)\\.(pdf|doc|docx)$");
	}
	public static boolean validateFileType2(String fileName) {

		return fileName.matches("(.*?)\\.(xl|xls|xlsx)$");
	}
	//
	public static boolean validateBlankSpacesInString(String name) {
		int count = 0;
		if (name.length() != 0) {
			for (int i = 0; i < name.length(); i++) {
				if (name.charAt(i) == ' ') {
					count++;
				}
			}
		}
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean validateConsecutiveBlankSpacesInString(String name) {
		int count = 0;
		
		if (name.length() != 0) {
			
			for (int i = 0; i < name.length() && i+1<name.length(); i++) {
				
					if ((name.charAt(i) == ' ') && (name.charAt(i+1) == ' ')) {
						
						count++;
					}
				
			}
		}
		if (count > 0) {
			return false;
		} else {
			return true;
		}
	}
	public static boolean validateFirstBlankSpace(String blankSpace) {
		if (blankSpace.charAt(0) == ' ') {
			return false;
		}
		return true;
	}
	public static boolean validateLastBlankSpace(String blankSpace) {
		int len=blankSpace.length();
		
		if (blankSpace.charAt(len-1) == ' ') {
			return false;
		}
		return true;
	}
	
	
	/***********************************Role Validations****************************/
	
	//Role Validations
		public static boolean validateRoleName(String rolename) {

			return rolename.matches("([a-zA-Z\\s]){1,60}");
		}
		
	/*******************************************************************************/

	/********************** User Validations ***************************************/
	
	// USER Validations

	public static boolean validateFirstName(String firstname) {

		return firstname.matches("([a-zA-Z\\s]){1,50}");
	}
	public static boolean validateKisanName(String firstname) {

		return firstname.matches("([a-zA-Z.\\s]){1,50}");
	}

	public static boolean validateLastName(String lastname) {

		return lastname.matches("([a-zA-Z\\s]){1,50}");
	}

	public static boolean validateUserAddress(String address) {

		//return address.matches("([a-zA-Z_#-\\s]){6,200}");
		return address
				.matches("^([a-zA-Z0-9(:)#-;,.\\s\\/]){0,200}$");
	}
	public static boolean validateAnnouncement(String content) {

		//return address.matches("([a-zA-Z_#-\\s]){6,200}");
		return content
				.matches("^([a-zA-Z0-9(:)#-;,.\\s\\/]){0,500}$");
	}

	public static boolean validateUserDepartment(String department) {

		return department.matches("([a-zA-Z&,\\s]){0,100}");
	}

	public static boolean validateUserDesignation(String designation) {

		return designation.matches("([a-zA-Z\\s]){0,50}");
	}
	public static boolean validateMPRN(String mpr) {

		return mpr.matches("([a-zA-Z0-9]){0,20}");
	}

	public static boolean validateVoterId(String str) {

		return str.matches("([a-zA-Z0-9\\/]){0,20}");
	}
	public static boolean validateUserMobile(String mobile) {

		return mobile.matches("\\d{10,15}");
		
	}
	
	public static boolean validateNumberOnly(String number) {

		return number.matches("^[0-9]+$");
	}
	public static boolean validateNumericValue(String number) {

		return number.matches("^[1-9]\\d*$");
	}
	public static boolean validateAlphanumericOnly(String str) {

		return str.matches("^[a-zA-Z0-9\\s]+$");
	}
	public static boolean validateKisanCard(String str) {

		return str.matches("^[a-zA-Z0-9]+$");
	}
	public static boolean validateIFSC(String str) {

		return str.matches("([a-zA-Z0-9]){11}");
	}
	public static boolean validateDecimalNumberOnly(String str) {

		return str.matches("^[0-9]\\d*(\\.\\d+)?$");
	}

	public static boolean validateUserPhone(String mobile) {

		return mobile.matches("\\d{10,11}");
	}

	public static boolean validateUserEmail(String userEmail) {

		return userEmail
				.matches("^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
	}
	public static boolean validateUserName(String userName) {
	
		return userName
				.matches("^[a-zA-Z0-9_.-]{2,50}$");
		
	}
	public static boolean validateUserNameStartingWithDigit(String userName) {
		
		return userName.startsWith("^([0-9]$");
		
	}

	
	public static boolean validatePassword(String password) {
		  pattern = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,20})");
		  matcher = pattern.matcher(password);
		return matcher.matches();
	}
	public static boolean validateUserID(String userID) {
		
		return userID
				.matches("^[a-zA-Z0-9_.-@]{2,50}$");
		
	}
	/*******************************************************************************/
	
	
	/************************* Manual Seeding Validations **************************/
	
	
	//manual seeding page
	public static boolean validateKisanUid(String uid) {
		  pattern = Pattern.compile("\\d{12,12}");
		  matcher = pattern.matcher(uid);
		return matcher.matches();
	}
	public static boolean validateSeedingLabelValue(String labelValue) {
		
		return labelValue
				.matches("^[a-zA-Z0-9]{0,20}$");
		
	}
	public static boolean validateSeedingDepartment(String department) {

		return department.matches("([a-zA-Z\\s]){0,50}");
	}
	public static boolean validateSeedingResidentLabel(String label) {
		return label.matches("([a-zA-Z\\s]){0,50}");
	}
	/*******************************************************************************/
	
/**********************ExternalDb Validations ***************************************/
	
	// ExternalDb Validations

	public static boolean validateExternalDbName(String extdbname) 
	{
		return extdbname.matches("([-a-zA-Z0-9' ']){1,50}");
	}
	
	public static boolean validateIpAdrress(String ipaddress) 
	{
		return ipaddress.matches("\\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.)"+ "{3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\b");
	}

	public static boolean validateSchema(String schema) 
	{
		return schema.matches("([a-zA-Z\\s]){1,50}");
	}

	public static boolean validateUrl(String url) 
	{
		return url.matches("^http[s]?://[-a-zA-Z0-9_.:]+[-a-zA-Z0-9_:@&?=+,.!/~*'%$]*$");
	}
	
	public static boolean validatePort(String port) 
	{
		return port.matches("(6553[0-5]|655[0-2]\\d|65[0-4]\\d{2}|6[0-4]\\d{3}|[1-5]\\d{4}|[1-9]\\d{0,3})" );
	}
	
	/*public static boolean validateFileName(String fileName){
		//return fileName.matches("([a-zAZ0-9]{1,200}\\.[a-zA-Z0-9] {1,10})");
		return fileName.matches("[^\\s]+(\\.(?i)(jpg|png|gif|bmp))$");
		//([^\s]+(\.(?i)(jpg|png|gif|bmp))$)
		
	}*/
	public static boolean validateFileName(String fileName){
		
		return fileName.matches("(^[a-zA-Z0-9\\_\\-]+)+\\.(pdf|ppt|doc|docx|xl|xls|xlsx)$");
		
		    //(^[a-zA-Z_\\-0-9]+)+\\.(pdf|ppt|doc|docx|xl|xls|xlsx)$
	}
	
	
	
	public static void main(String args[]){
	
		//System.out.println(validateVoterId("WB/06/035/1414344"));

		}

}

