package com.ajatus.orims;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ajatus.model.CollectionHoldingTax;
import com.ajatus.model.DemandWard;
import com.ajatus.model.DemandWardMonth;
import com.ajatus.model.HoldingTaxModel;
import com.ajatus.model.HoldingTaxPast;
import com.ajatus.model.NoOfHolding;
import com.ajatus.model.WardOffice;
import com.ajatus.dto.HoldingTaxDto;
import com.ajatus.service.CollectionHoldingTaxService;
import com.ajatus.service.DistrictService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.HoldingTaxPastService;
import com.ajatus.service.MonthService;
import com.ajatus.service.NoOfHoldingService;
import com.ajatus.service.TaxCollectorService;
import com.ajatus.service.UlbService;
import com.ajatus.service.WardOfficeService;
import com.ajatus.service.WardService;

@Controller
@RequestMapping(value="/collection_holding_tax")
public class CollectionHoldingTaxController {
	
	
	@Autowired(required=true)
	private CollectionHoldingTaxService collectionHoldingTaxService;

	@Autowired(required=true)
	private DistrictService districtService;
	
	@Autowired(required=true)
	private FinancialYearService financialYearService;
	
	@Autowired(required=true)
	private WardService wardService;
	
	@Autowired(required=true)
	private UlbService ulbService;
	
	@Autowired(required=true)
	private MonthService monthService ;
	
	@Autowired(required=true)
	private WardOfficeService wardOfficeService;
	
	@Autowired(required=true)
	private TaxCollectorService taxCollectorService;
	
	@Autowired(required=true)
	private NoOfHoldingService noOfHoldingService;
	
	@Autowired(required=true)
	private HoldingTaxPastService holdingTaxPastService;
	
	@RequestMapping(value="/view")
	public String listPropertyTaxes(ModelMap model){
		
		model.addAttribute("listCollectionHoldingTaxes",this.collectionHoldingTaxService.listCollectionHoldingTaxes());
		
		return "collection_holding_tax.view";
	}
	
	@RequestMapping(value="/new_entry_form")
	public String addPropertytax(ModelMap model){
		
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("wards", this.wardService.listWards());
		model.addAttribute("wardOffices", this.wardOfficeService.listWardOffices());
		model.addAttribute("collectionHoldingTax", new CollectionHoldingTax());
		model.addAttribute("taxCollectors", this.taxCollectorService.listTaxCollectors());
		
		return "collection_holding_tax.entry_form";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveHoldingTax(@ModelAttribute CollectionHoldingTax h){
		
		this.collectionHoldingTaxService.addCollectionHoldingTax(h);
		return "redirect:/collection_holding_tax/view";
	}
	
	@RequestMapping("/remove/{id}")
    public String removeDemandWard(@PathVariable("id") int id){
		
        this.collectionHoldingTaxService.removeCollectionHoldingTax(id);
        return "redirect:/collection_holding_tax/view";
    }
	

	@RequestMapping(value = "/past_collection",method=RequestMethod.GET)
	public String holdingPast(Model model)
	{
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("month",this.monthService.listMonths());		
		model.addAttribute("holdingTaxPast",new HoldingTaxPast());
		return "holding.past";
	}
	
	@RequestMapping(value = "/past_collection",method=RequestMethod.POST)
	public String PersistHoldingPast(Model model,@ModelAttribute("holdingTaxPast") HoldingTaxPast holdingTaxPast)
	{
		this.holdingTaxPastService.addHoldingTaxPast(holdingTaxPast);
		return "redirect:/collection_holding_tax/past_collection";
	}
	@RequestMapping(value = "/no_of_holdings",method=RequestMethod.GET)  
	public String noOfHolding(Model model)
	{
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());     
		model.addAttribute("ward", this.wardService.listWards());
		model.addAttribute("holdingCount",new NoOfHolding());
		model.addAttribute("holdingCount",new NoOfHolding());
		return "holding.count";
	} 
	@RequestMapping(value="/no_of_holdings",method=RequestMethod.POST)
	public String persistNoOfHolding(Model model,@Valid @ModelAttribute("holdingCount") NoOfHolding noOfHolding, BindingResult bindingResult)
	{
		if(bindingResult.hasErrors())
		{
			model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
			model.addAttribute("districts",this.districtService.listDistricts());
			model.addAttribute("ulbs",this.ulbService.listUlbs());
			model.addAttribute("ward", this.wardService.listWards());
			model.addAttribute("holdingCount",noOfHolding);
			//System.out.println("validation successful"+bindingResult.getFieldErrors());
			return "holding.count";
		}
		else{
			this.noOfHoldingService.addNoOfHolding(noOfHolding);
		}
		
		return "redirect:/collection_holding_tax/no_of_holdings";
	}

}
