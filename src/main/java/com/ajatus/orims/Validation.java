package com.ajatus.orims;

import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;

import com.ajatus.model.AssetParkingLotAuctioned;
import com.ajatus.model.AssetParkingLotULB;
import com.ajatus.model.BigDefaultersModel;
import com.ajatus.model.ParkingLots;
import com.ajatus.model.TaxRates;
import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.model.TradeLicenceCollectionModel;
import com.ajatus.model.TradeLicenceCollectionPastModel;
import com.ajatus.model.TradeLicenceDemandModel;

@Validated
public class Validation {

	public boolean  isValidData(Model model, ParkingLots parkinglots) {
		
		boolean flag =false;
		if(parkinglots.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(parkinglots.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(parkinglots.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
		
	}

	public boolean isValidData(Model model, TradeBigDefaultersModel tradeBigDefaultersModel) {
		boolean flag =false;
		if(tradeBigDefaultersModel.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(tradeBigDefaultersModel.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(tradeBigDefaultersModel.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, TradeLicence tLicence) {
		boolean flag =false;
		if(tLicence.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		
		if(tLicence.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, TaxRates taxrate) {
		boolean flag =false;
		if(taxrate.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		
		if(taxrate.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, BigDefaultersModel bDefaultersModel) {
		boolean flag =false;
		if(bDefaultersModel.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(bDefaultersModel.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(bDefaultersModel.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, AssetParkingLotAuctioned assetParkingLotAuctioned) {
		boolean flag =false;
		if(assetParkingLotAuctioned.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(assetParkingLotAuctioned.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(assetParkingLotAuctioned.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, AssetParkingLotULB asParkingLotULB) {
		boolean flag =false;
		if(asParkingLotULB.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(asParkingLotULB.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(asParkingLotULB.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, TradeLicenceCollectionPastModel tradeLicenceCollectionPastModel) {
		boolean flag =false;
		if(tradeLicenceCollectionPastModel.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(tradeLicenceCollectionPastModel.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(tradeLicenceCollectionPastModel.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, TradeLicenceCollectionModel tradeLicenceCollectionModel) {
		boolean flag =false;
		if(tradeLicenceCollectionModel.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(tradeLicenceCollectionModel.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(tradeLicenceCollectionModel.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}

	public boolean isValidData(Model model, TradeLicenceDemandModel tradeLicenceDemandModel) {
		boolean flag =false;
		if(tradeLicenceDemandModel.getDistrict().getId()!=0) {
			model.addAttribute("district_error", "Please select  district."); flag =true; }
		if(tradeLicenceDemandModel.getWard().getId()!=0) {
			model.addAttribute("ward_error", "Please select  ward."); flag =true; }
		
		if(tradeLicenceDemandModel.getUlb().getId()!=0) {  
			model.addAttribute("ulb_error", "Please select  Ulb."); flag =true; }
		
		return flag;
	}
	
	
	public static boolean validateName(String name) {

		return name.matches("([a-zA-Z.'\\s]){3,60}");
	}
	

}
