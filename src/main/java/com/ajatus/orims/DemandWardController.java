package com.ajatus.orims;

import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ajatus.model.CollectionAdvertismentTaxes;
import com.ajatus.model.DemandWard;
import com.ajatus.model.DemandWardMonth;
import com.ajatus.service.DemandWardService;
import com.ajatus.service.DistrictService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.MonthService;
import com.ajatus.service.UlbService;
import com.ajatus.service.WardService;

@Controller
@RequestMapping(value="/demand_ward")
public class DemandWardController {

	private static final Logger logger = LoggerFactory.getLogger(DemandWardController.class);
	
	@Autowired(required=true)
	private DemandWardService demandWardService;
	@Autowired(required=true)
	private FinancialYearService financialYearService;
	@Autowired(required=true)
	private DistrictService districtService;
	@Autowired(required=true)
	private UlbService ulbService;
	@Autowired(required=true)
	private WardService wardService;
	@Autowired(required=true)
	private MonthService monthService;
		
	@RequestMapping(value="/view")
	public String view(ModelMap model){
		
		model.addAttribute("listDemandWards",this.demandWardService.listDemandWards());
		
		return "demand_ward.view";
	}
	
	@RequestMapping(value="/new_entry_form")
	public String newEntryForm(ModelMap model){
		
		model.addAttribute("demandWard",new DemandWard());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		model.addAttribute("months",this.monthService.listMonths());
		
		return "demand_ward.entry_form";
	}
	
	@RequestMapping(value="/edit_entry_form/{id}")
	public String editEntryForm(@PathVariable("id") int id,Model model){	
		DemandWard demandWard = demandWardService.getDemandWardById(id);    
		//if(!demandWard.getIsSetMonthly()) demandWard.setDemandWardMonths(this.monthService.listMonths());
		model.addAttribute("demandWard",demandWard);      
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		model.addAttribute("demandWardMonths",demandWard.getDemandWardMonths());
		model.addAttribute("months",this.monthService.listMonths());   
		
		
		/*model.addAttribute("demandWard",this.demandWardService.getDemandWardById(id));
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		System.out.println("-------------==============-------------------------");
		System.out.println(this.monthService.listMonths());  
		model.addAttribute("months",this.monthService.listMonths());*/
		return "demand_ward.edit_entry_form";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)           
	public String editDemandWard(@ModelAttribute("demandWard") DemandWard dw,BindingResult result){
		this.demandWardService.updateDemandWard(dw);
		return "redirect:/demand_ward/view";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveDemandWard(@ModelAttribute DemandWard dw,Model model){
		
		Boolean boleab =demandWardService.validateWardFinancialYear(dw.getWard().getId(), dw.getFinancialYear().getId());
		System.out.println(boleab);
		if(!boleab){
			model.addAttribute("error_msg","Demand already exist for this Ward and Financial Year");
			model.addAttribute("demandWard",dw);
			model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
			model.addAttribute("districts",this.districtService.listDistricts());
			model.addAttribute("ulbs",this.ulbService.listUlbs());
			model.addAttribute("wards",this.wardService.listWards());
			model.addAttribute("months",this.monthService.listMonths());
			return "demand_ward.entry_form";  
		}else{
			
			if(!dw.getIsSetMonthly()){
				dw.getDemandWardMonths().clear();
			}
			
			for (DemandWardMonth demandWardMonth : dw.getDemandWardMonths()) {
				demandWardMonth.setDemandWard(dw);
			}
				
			logger.debug("demand ward : "+dw.getDemandWardMonths().size());
			this.demandWardService.addDemandWard(dw);
			return "redirect:/demand_ward/view";
		}
				
		
	}
	
	@RequestMapping("/remove_demand_ward/{id}")
    public String removeDemandWard(@PathVariable("id") int id){
		
        this.demandWardService.removeDemandWard(id);
        return "redirect:/demand_ward/view";
    }
	
}
