package com.ajatus.orims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.AdvertismentTypes;
import com.ajatus.model.CollectionAdvertismentTaxes;
import com.ajatus.model.FinancialYear;
import com.ajatus.service.AdvertismentTypesService;
import com.ajatus.service.CollectionAdvertismentTaxesService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.UlbService;
import com.ajatus.service.WardService;

@Controller
@RequestMapping(value="/collection_advertisment_taxes")
public class CollectionAdvertismentTaxesController {

	@Autowired(required=true)
	private CollectionAdvertismentTaxesService collectionAdvertismentTaxesService;
	
	@Autowired(required=true)
	private FinancialYearService financialYearService;
	
	@Autowired(required=true)
	private AdvertismentTypesService advertismentTypesService;
	
	@Autowired(required=true)
	private UlbService ulbService;
	
	@Autowired(required=true)
	private WardService wardService;
		
	@RequestMapping(value="/view")
	public String view(ModelMap model){
		model.addAttribute("listcollectionAdvertismentTaxes", this.collectionAdvertismentTaxesService.listCollectionAdvertismentTaxes());
		return "collection_advertisment_taxes.view";
	}
	
	@RequestMapping(value = "/new_entry_form")
	public String newEntryForm(ModelMap model) {
		model.addAttribute("collectionAdvertismentTaxes",new CollectionAdvertismentTaxes());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		model.addAttribute("advertismentTypes",this.advertismentTypesService.listAdvertismentType());
		return "collection_advertisment_taxes.new_entry_form";
	}
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveCollectionAdvertismentTaxes(@ModelAttribute CollectionAdvertismentTaxes cat){
		this.collectionAdvertismentTaxesService.addCollectionAdvertismentTaxes(cat);
		return "redirect:/collection_advertisment_taxes/view";
	}
	
	@RequestMapping(value="/edit_entry_form/{id}")
	public String editEntryForm(@PathVariable("id") int id,Model model){
		 model.addAttribute("collectionAdvertismentTaxes", this.collectionAdvertismentTaxesService.listCollectionAdvertismentTaxesById(id));
		 model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		 model.addAttribute("ulbs",this.ulbService.listUlbs());
		 model.addAttribute("wards",this.wardService.listWards());
		 model.addAttribute("advertismentTypes",this.advertismentTypesService.listAdvertismentType());

		return "collection_advertisment_taxes.edit_entry_form";  
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)           
	public String editCollectionAdvertismentTaxes(@ModelAttribute("collectionAdvertismentTaxes") CollectionAdvertismentTaxes cat,BindingResult result){
		this.collectionAdvertismentTaxesService.updateCollectionAdvertismentTaxes(cat);
		return "redirect:/collection_advertisment_taxes/view";
	}
	
	@RequestMapping("/remove_advertisment_types/{id}")
    public String removeCollectionAdvertismentTaxes(@PathVariable("id") int id){
		
        this.collectionAdvertismentTaxesService.removeCollectionAdvertismentTaxes(id);
        return "redirect:/collection_advertisment_taxes/view";
    }
	
}
