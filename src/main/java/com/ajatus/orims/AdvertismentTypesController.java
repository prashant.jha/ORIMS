package com.ajatus.orims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.AdvertismentTypes;
import com.ajatus.model.CollectionHoldingTax;
import com.ajatus.service.AdvertismentTypesService;

@Controller
@RequestMapping(value="/advertisment_types")
public class AdvertismentTypesController {
	
	@Autowired(required=true)
	private AdvertismentTypesService advertismentTypesService;
	
	@RequestMapping(value="/view")
	public String view(ModelMap model){
		model.addAttribute("listAdvertismentTypes", this.advertismentTypesService.listAdvertismentType());
		return "advertisment_types.view";
	}
		
	@RequestMapping(value = "/new_entry_form")
	public String newEntryForm(ModelMap model) {
		model.addAttribute("advertismentTypes",new AdvertismentTypes());
		return "advertisment_types.new_entry_form";
	}
	
	@RequestMapping(value="/edit_entry_form/{id}")
	public String editEntryForm(ModelMap model){
		
		model.addAttribute("advertismentTypes",new AdvertismentTypes());
		
		return "advertisment_types.new_entry_form";
	}
	
	@RequestMapping("/remove_advertisment_types/{id}")
    public String removeAdvertismentTypes(@PathVariable("id") int id){
		
        this.advertismentTypesService.removeAdvertismentType(id);
        return "redirect:/advertisment_types/view";
    }
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveAdvertismentTypes(@ModelAttribute AdvertismentTypes at){
		this.advertismentTypesService.addAdvertismentType(at);
		return "redirect:/advertisment_types/view";
	}

}
