package com.ajatus.orims;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.DemandTaxCollector;
import com.ajatus.model.DemandTaxCollectorMonth;
import com.ajatus.model.DemandWard;
import com.ajatus.model.DemandWardMonth;
import com.ajatus.service.DemandTaxCollectorService;
import com.ajatus.service.DemandWardService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.MonthService;
import com.ajatus.service.TaxCollectorService;
import com.ajatus.service.WardService;

@Controller
@RequestMapping(value="/demand_taxcollector")
public class DemandTaxCollectorController {

	private static final Logger logger = LoggerFactory.getLogger(DemandTaxCollectorController.class);
	
	@Autowired(required=true)
	private WardService wardService;
	@Autowired(required=true)
	private DemandTaxCollectorService demandTaxCollectorService;
	@Autowired(required=true)
	private TaxCollectorService taxCollectorService;
	@Autowired(required=true)
	private DemandWardService demandWardService;
	@Autowired(required=true)
	private MonthService monthService;

	@Autowired(required=true)
	private FinancialYearService financialYearService;
	
	@RequestMapping(value="/view")
	public String view(ModelMap model){
		
		model.addAttribute("listTaxCollectorDemands",this.demandTaxCollectorService.listDemandTaxCollectors());
		
		return "demand_taxcollector.view";
	}
	
	@RequestMapping(value = "/new_entry_form")
	public String newEntryForm(ModelMap model) {  
		
		model.addAttribute("demandTaxCollector", new DemandTaxCollector());
		model.addAttribute("demandWards",this.demandWardService.listDemandWards());
		model.addAttribute("wards", this.wardService.listWards());
		model.addAttribute("taxCollectors",this.taxCollectorService.listTaxCollectors());
		model.addAttribute("months",this.monthService.listMonths());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears()); 
		return "demand_taxcollector.entry_form";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveDemandWard(@ModelAttribute DemandTaxCollector dw,Model model){
		
					
			if(!dw.getIsSetMonthly()){
				dw.getDemandTaxCollectorMonths().clear();;
			}
			
			for (DemandTaxCollectorMonth demandTaxCollectorMonth : dw.getDemandTaxCollectorMonths()) {
				demandTaxCollectorMonth.setDemandTaxCollector(dw);
			}
				
			logger.debug("demand ward : "+dw.getDemandTaxCollectorMonths().size());
			this.demandTaxCollectorService.addDemandTaxCollector(dw);
			return "redirect:/demand_taxcollector/new_entry_form";             
		}
				
	
	
	@RequestMapping(value="/edit_entry_form/{id}")
	public String editEntryForm(@PathVariable("id") int id,Model model){	
		model.addAttribute("demandTaxCollector",this.demandTaxCollectorService.getDemandTaxCollectorById(id));
		//model.addAttribute("demandTaxCollector",new DemandTaxCollector());
		model.addAttribute("demandWards",this.demandWardService.listDemandWards());
		model.addAttribute("wards", this.wardService.listWards());
		model.addAttribute("taxCollectors",this.taxCollectorService.listTaxCollectors());
		model.addAttribute("months",this.monthService.listMonths());
		return "demand_taxcollector.edit_entry_form";
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)           
	public String editDemandWard(@ModelAttribute("demandTaxCollector") DemandTaxCollector dtc,BindingResult result){
		this.demandTaxCollectorService.updateTaxCollector(dtc);
		return "redirect:/demand_taxcollector/view";
	}
}
