package com.ajatus.orims;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.ajatus.service.UserService;
import com.ajatus.service.CustomUserDetails;
import com.ajatus.jpa.UserJPA;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.validation.BindingResult;


import com.ajatus.dao.UserDAOImpl;
import com.ajatus.model.User;
import com.ajatus.service.UserService;
import com.ajatus.service.UserTypeService;

@Controller
public class UsersController {
	
	@Autowired
	private UserJPA userJPA;
	
	private static final Logger logger = LoggerFactory.getLogger(UsersController.class);
	
	private UserService UserService;
	private UserTypeService UserTypeService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired(required=true)
	@Qualifier(value="userService")
	public void setUsersService(UserService us){
		this.UserService = us;
	}
	
	@Autowired(required=true)
	@Qualifier(value="userTypeService")
	public void setUsersTypeService(UserTypeService uts){
		this.UserTypeService = uts;
	}
	
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
		
		
		 
		
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.GET)
    public String passwordForm(Model model,String errorOldPassword, String errorConfrimPassword, String success) {
		if(errorOldPassword!= null)
			model.addAttribute("errorOldPassword", "Old Password don't march with the stored one");
		if(errorConfrimPassword!= null)
			model.addAttribute("errorConfrimPassword", "The Password Dont Match");
		if(success!= null)	
			model.addAttribute("success", "Password Changed Successfully");
		
		String loggedinAuth = null;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	    Collection<? extends GrantedAuthority> Auth= auth.getAuthorities();
	    for (GrantedAuthority grantedAuthority : Auth) {
			 loggedinAuth = grantedAuthority.toString();
			
		}
	    
	  //  logger.info("role:"+loggedinAuth);
	    if (loggedinAuth.equals("admin"))
	    	return "passwordForm.admin";	
	    else
	    	return "passwordForm";
	   
    }
	
	@RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public String changePassword(@ModelAttribute("User") User u,@RequestParam("oldPassword") String oldPassword,@RequestParam("newPassword") String newPassword,@RequestParam("confirmPassword") String confirmPassword) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	      String name = auth.getName(); //get logged in username
		 u = userJPA.findByUserName(name);
		 String encodedPassword = u.getPasswordHash();
		 if(!newPassword.equals(confirmPassword))
		 {
			// logger.info(newPassword);
			// logger.info("##"+confirmPassword);
			 return "redirect:/changePassword?errorConfrimPassword";
		 }
		 else if(bCryptPasswordEncoder.matches(oldPassword, encodedPassword))
			 {
			     u.setPasswordHash(bCryptPasswordEncoder.encode(newPassword));
			     this.UserService.updateUser(u);
			     return "redirect:/changePassword?success";
			     
			 }
		 else{
			  return "redirect:/changePassword?errorOldPassword";
		     }
		
    }
	
	@RequestMapping(value = "/Users", method = RequestMethod.GET)
	public String listUsers(Model model) {
		model.addAttribute("User", new User());
		model.addAttribute("listUsers", this.UserService.listUsers());
		return "Users";
	}
	
	//For add and update UsersType both
	@RequestMapping(value= "/User/add", method = RequestMethod.POST)
	public String addUsers(@ModelAttribute("User") User u){
		Date date = new Date();
		u.setCreatedAt(date);
		u.setUpdatedAt(date);
		u.setIsDeleted(true);
		u.setResetToken("");
		u.setStatus(true);
		u.setUserType(UserTypeService.getUserTypeById(1));
		if(u.getId() == 0){
			//new User, add it
			this.UserService.addUser(u);
		}else{
			//existing User, call update
			this.UserService.updateUser(u);
		}
		
		return "redirect:/Users";
		
	}
	
	@RequestMapping("/removeUser/{id}")
    public String removeUsers(@PathVariable("id") int id){
		
        this.UserService.removeUser(id);
        return "redirect:/Users";
    }
 
    @RequestMapping("/editUser/{id}")
    public String editUsersType(@PathVariable("id") int id, Model model){
        model.addAttribute("User", this.UserService.getUserById(id));
        model.addAttribute("listUsers", this.UserService.listUsers());
        return "Users";
    }
	
}
