package com.ajatus.orims;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.MarketRentalCollection;
import com.ajatus.model.MarketRentalCollectionPast;
import com.ajatus.model.MarketRentalProperties;
import com.ajatus.dto.HoldingTaxDto;
import com.ajatus.dto.MarketRental;
import com.ajatus.service.DistrictService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.MarketRentalService;
import com.ajatus.service.UlbService;

@Controller
@RequestMapping(value="/market_rental")
public class MarketRentalController {
	@Autowired(required=true)
	private DistrictService districtService;
	@Autowired(required=true)
	private UlbService ulbService;
	@Autowired(required=true)
	private FinancialYearService financialYearService;
	
	@Autowired(required=true)
	private MarketRentalService marketRentalService;
	private static final Logger logger = LoggerFactory.getLogger(MarketRentalController.class);
	
	@RequestMapping(value = "/market_rental_add",method=RequestMethod.GET)  
	public String noOfRental(Model model)
	{
		
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("marketRental",new MarketRentalProperties());
		return "market_rental.count";
	} 
	@RequestMapping(value="/market_rental_add",method=RequestMethod.POST)
	public String persistNoOfRental(Model model,@Valid @ModelAttribute("marketRental") MarketRentalProperties mr, BindingResult bindingResult)
	{
		this.marketRentalService.addMarketRentalProperties(mr);		
		return "redirect:/market_rental/market_rental_add";
	}
	
	
	@RequestMapping(value = "/muncipal_properties_collection",method=RequestMethod.GET)  
	public String muncipalPropertiesCollection(Model model)
	{
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("marketRental",new MarketRentalCollectionPast());
		return "muncipal.properties.collection";
	} 
	
	
	@RequestMapping(value="/muncipal_properties_collection",method=RequestMethod.POST)
	public String persistMuncipalPropertiesCollection(Model model,@Valid @ModelAttribute("marketRentalCollectionPast") MarketRentalCollectionPast marketRentalCollectionPast, BindingResult bindingResult)
	{
		this.marketRentalService.addMarketRentalCollectionPast(marketRentalCollectionPast);
		return "redirect:/market_rental/muncipal_properties_collection";
	}
	
	
	@RequestMapping(value = "/muncipal_properties_collection_daily",method=RequestMethod.GET)  
	public String muncipalPropertiesCollectionDaily(Model model)
	{
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("marketRental",new MarketRental());
		return "muncipal.properties.collection.daily";
	} 
	@RequestMapping(value="/muncipal_properties_collection_daily",method=RequestMethod.POST)
	public String persistMuncipalPropertiesCollectiondaily(Model model,@Valid @ModelAttribute("marketRentalCollection") MarketRentalCollection marketRentalCollection, BindingResult bindingResult)
	{
		this.marketRentalService.addMarketRentalCollection(marketRentalCollection);
		return "redirect:/market_rental/muncipal_properties_collection_daily";
	}
	
	
}

