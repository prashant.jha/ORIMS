package com.ajatus.orims;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ajatus.model.Ulb;
import com.ajatus.model.Ward;
import com.ajatus.model.Zone;
import com.ajatus.service.UlbService;
import com.ajatus.service.WardService;
import com.ajatus.service.ZoneService;
import com.google.gson.Gson;

@Controller
public class CommonController {

	@Autowired(required=true)
	private UlbService ulbService;
	@Autowired(required=true)
	private WardService wardService;
	@Autowired(required=true)
	private ZoneService zoneService ;
	
	@RequestMapping(value = "/getUlbFromDistrict/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUlbByDistrict(Locale Local, Model model,@PathVariable("id") int district_id){
		 List<Ulb> ulbList= this.ulbService.listUlbsByDistrict(district_id);
		 Gson gson = new Gson();
		  String json = gson.toJson(ulbList);
		 return json;
		
	}
	@RequestMapping(value = "/getWardFromUlb/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getWardByUlb(Locale Local, Model model,@PathVariable("id") int ulb_id){
		 List<Ward> wardList= this.wardService.listWardsByUlb(ulb_id);
		 Gson gson = new Gson();
		  String json = gson.toJson(wardList);
		 return json;
		
	}
	
	@RequestMapping(value = "/getZoneFromUlb/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getZoneByUlb(Locale Local, Model model,@PathVariable("id") int ulb_id){
		 List<Zone> zoneList= this.zoneService.listZonesByUlb(ulb_id);
		 Gson gson = new Gson();
		  String json = gson.toJson(zoneList);
		 return json;
		
	}
	
}
