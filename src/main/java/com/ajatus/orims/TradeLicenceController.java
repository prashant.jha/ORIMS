package com.ajatus.orims;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.model.TradeLicenceCollectionModel;
import com.ajatus.model.TradeLicenceCollectionPastModel;
import com.ajatus.model.TradeLicenceDemandModel;
import com.ajatus.service.DistrictService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.TradeLicenceService;
import com.ajatus.service.UlbService;
import com.ajatus.service.WardService;

@Controller
@RequestMapping(value="/trade_licence")
public class TradeLicenceController {
	@Autowired(required=true)
	private FinancialYearService financialYearService;
	@Autowired(required=true)
	private DistrictService districtService;
	@Autowired(required=true)
	private UlbService ulbService;
	@Autowired(required=true)
	private WardService wardService;
	private Validation validation = new Validation();
	
	@Autowired(required=true)
	private TradeLicenceService tradeLicenceService;
	
	@RequestMapping(value = "/trade_licence_demand" , method = RequestMethod.GET)
    public String tradeLicenceDemand(Model model){
	
		model.addAttribute("tradeLicenceDemand" ,new TradeLicenceDemandModel());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		
        return "trade_licence_demand";  
    }
	
	
	@RequestMapping(value = "/trade_licence_demand" , method = RequestMethod.POST)
    public String addTradeLicenceDemand(Model model,@ModelAttribute("tradeLicenceDemand")TradeLicenceDemandModel tradeLicenceDemandModel){
		
		tradeLicenceService.addTradeLicenceDemand(tradeLicenceDemandModel);
		
		model.addAttribute("tradeLicenceDemand" ,tradeLicenceDemandModel);
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
	
		/*if(validation.isValidData(model, tradeLicenceDemandModel)){
			return "trade_licence_demand";
		}*/
		
		
        return "redirect:/trade_licence/trade_licence_demand";
    }
	
	@RequestMapping(value = "/trade_licence_collection" , method = RequestMethod.GET)
    public String tradeLicenceCollection(Model model){
	
		model.addAttribute("tradeLicenceCollection" ,new TradeLicenceCollectionModel());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		
		
        return "trade_licence_collection";
    }
	
	
	@RequestMapping(value = "/trade_licence_collection" , method = RequestMethod.POST)
    public String addTradeLicenceCollection(Model model,@ModelAttribute("tradeLicenceCollection")TradeLicenceCollectionModel tradeLicenceCollectionModel){
	
		
		tradeLicenceService.addTradeLicenceCollection(tradeLicenceCollectionModel);
		
		model.addAttribute("tradeLicenceCollection" ,tradeLicenceCollectionModel);
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		
		/*if(validation.isValidData(model, tradeLicenceCollectionModel)){
			return "trade_licence_collection";
		}*/
		
		
        return "redirect:/trade_licence/trade_licence_collection";
    }
	
	@RequestMapping(value = "/trade_licence_collection_past" , method = RequestMethod.GET)
    public String tradeLicenceCollectionPast(Model model){
	
		model.addAttribute("tradeLicenceCollectionPast" ,new TradeLicenceCollectionPastModel());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		
		
        return "trade_licence_collection_past";
    }
	
	
	@RequestMapping(value = "/trade_licence_collection_past" , method = RequestMethod.POST)
    public String addTradeLicenceCollectionPast(Model model,@ModelAttribute("tradeLicenceCollectionPast")TradeLicenceCollectionPastModel tradeLicenceCollectionPastModel){
		tradeLicenceService.addTradeLicenceCollectionPast(tradeLicenceCollectionPastModel);
		model.addAttribute("tradeLicenceCollection" ,tradeLicenceCollectionPastModel);
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
	
		
		/*if(validation.isValidData(model, tradeLicenceCollectionPastModel)){
			return "trade_licence_collection_past";
		}*/
		
        return "redirect:/trade_licence/trade_licence_collection_past";
    }
	
	@RequestMapping(value = "/no_trade_licence" , method = RequestMethod.GET)
    public String noTradeLicence(Model model){
		model.addAttribute("tradeLicence" ,new TradeLicence());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
        return "no_trade_licence";
    }
	
	
	@RequestMapping(value = "/no_trade_licence" , method = RequestMethod.POST)
    public String addNoTradeLicence(Model model,@ModelAttribute("tradeLicence")TradeLicence tLicence){
		tradeLicenceService.addNoTradeLicence(tLicence);
		model.addAttribute("tradeLicence" ,tLicence);
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
	
		/*if(validation.isValidData(model, tLicence)){
			return "no_trade_licence";
		}*/
		
        return "redirect:/trade_licence/no_trade_licence";
    }
	
	@RequestMapping(value = "/trade_big_defaulters" , method = RequestMethod.GET)
    public String tradeBigDefaulters(Model model){
		model.addAttribute("tradeBigDefaulters" ,new TradeBigDefaultersModel());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
        return "trade_big_defaulters";
    }
	
	
	@RequestMapping(value = "/trade_big_defaulters" , method = RequestMethod.POST)
    public String addTradeBigDefaulters(Model model,@ModelAttribute("tradeBigDefaulters")TradeBigDefaultersModel tradeBigDefaultersModel){
		model.addAttribute("tradeBigDefaulters" ,tradeBigDefaultersModel);
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
	
		tradeLicenceService.addTradeBigDefaulters(tradeBigDefaultersModel);
		
		/*if(validation.isValidData(model, tradeBigDefaultersModel)){
			return "trade_big_defaulters";
		}*/
		
        return "redirect:/trade_licence/trade_big_defaulters";   
    }
	
}
