package com.ajatus.orims;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.UserType;
import com.ajatus.service.UserTypeService;

@Controller
public class UserTypesController {
	
	private UserTypeService UserTypeService;
	
	@Autowired(required=true)
	@Qualifier(value="userTypeService")
	public void setUsersTypeService(UserTypeService ps){
		this.UserTypeService = ps;
	}
	
	@RequestMapping(value = "/UserTypes", method = RequestMethod.GET)
	public String listUsersTypes(Model model) {
		model.addAttribute("UserType", new UserType());
		model.addAttribute("listUserTypes", this.UserTypeService.listUserTypes());
		return "UserTypes";
	}
	
	//For add and update UsersType both
	@RequestMapping(value= "/UserType/add", method = RequestMethod.POST)
	public String addUsersType(@ModelAttribute("UserType") UserType p){
		Date date = new Date();
		p.setCreatedAt(date);
		p.setUpdatedAt(date);
		p.setStatus(true);
		if(p.getId() == 0){
			//new UsersType, add it
			this.UserTypeService.addUserType(p);
		}else{
			//existing UsersType, call update
			this.UserTypeService.updateUserType(p);
		}
		
		return "redirect:/UserTypes";
		
	}
	
	@RequestMapping("/removeUserType/{id}")
    public String removeUsersType(@PathVariable("id") int id){
		
        this.UserTypeService.removeUserType(id);
        return "redirect:/UserTypes";
    }
 
    @RequestMapping("/editUserType/{id}")
    public String editUsersType(@PathVariable("id") int id, Model model){
        model.addAttribute("UserType", this.UserTypeService.getUserTypeById(id));
        model.addAttribute("listUserTypes", this.UserTypeService.listUserTypes());
        return "UserTypes";
    }
	
}
