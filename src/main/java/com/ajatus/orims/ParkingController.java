package com.ajatus.orims;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ajatus.model.AssetParkingLotAuctioned;
import com.ajatus.model.AssetParkingLotULB;
import com.ajatus.model.BigDefaultersModel;
import com.ajatus.model.ParkingLots;
import com.ajatus.model.ParkingLotsPast;
import com.ajatus.model.TaxRates;
import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.service.DistrictService;
import com.ajatus.service.FeeCollectorService;
import com.ajatus.service.FinancialYearService;
import com.ajatus.service.ParkingService;
import com.ajatus.service.UlbService;
import com.ajatus.service.WardService;
import com.ajatus.service.ZoneService;

@Controller
@RequestMapping(value="/parking")
public class ParkingController {   

	@Autowired(required=true)
	private FinancialYearService financialYearService;
	@Autowired(required=true)
	private DistrictService districtService;
	@Autowired(required=true)
	private UlbService ulbService;
	
	@Autowired(required=true)
	private ZoneService zoneService;
	@Autowired(required=true)
	private FeeCollectorService feeCollectorService;

	@Autowired(required=true)
	private ParkingService parkingService;
	
	@Autowired(required=true)
	private WardService wardService;
	
	private Validation validation = new Validation();
	
	
	@RequestMapping(value = "/parking_lots" , method = RequestMethod.GET)
    public String parkingLots(Model model){
	
		model.addAttribute("parking" ,new ParkingLots());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("zones",this.zoneService.listZones());
		model.addAttribute("wards",this.wardService.listWards());
		model.addAttribute("feeCollector",this.feeCollectorService.listFeeCollector());
		
        return "parking_lots";
    }
	
	
	@RequestMapping(value = "/parking_lots" , method = RequestMethod.POST)
    public String parkingLotsAdd(Model model, @ModelAttribute("parking")ParkingLots parkingLots,BindingResult error){
		
		
		
		
		model.addAttribute("parking" ,parkingLots);
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("zones",this.zoneService.listZones());
		model.addAttribute("wards",this.wardService.listWards());
	
		parkingService.addParkingLots(parkingLots);
		/*Validation validation = new Validation();
		if(validation.isValidData(model, parkingLots)){
			return "parking_lots";
		}*/
	//	parkingService.addParkingLots(parkingLots);
		
        return "redirect:/parking/parking_lots";
    }
	
	
	@RequestMapping(value = "/parking_lot_past", method = RequestMethod.GET)
	public String parkingLotPast(Model model)
	{
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("parkingLotsPast",new ParkingLotsPast());
		return "parking_lot_past";
	}
	
	@RequestMapping(value = "/parking_lot_past", method = RequestMethod.POST)
	public String addParkingLotPast(Model model,@ModelAttribute("parkingLotsPast")ParkingLotsPast parkingLotsPast )
	{
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("districts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("parkingLotsPast",parkingLotsPast);
		
		
		parkingService.addParkingLotsPast(parkingLotsPast);
		
		return "redirect:/parking/parking_lot_past";   
	}
	
	
	
	@RequestMapping(value = "/asset_parkingLotULB" , method = RequestMethod.GET)
    public String assetParkingLotULB(Model model){
		model.addAttribute("assetParkingLotULB" ,new AssetParkingLotULB());
		model.addAttribute("zones",this.zoneService.listZones());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
		
        return "asset_parking_lot_ULB";
    }
	
	
	@RequestMapping(value = "/asset_parkingLotULB" , method = RequestMethod.POST)
    public String addAssetParkingLotULB(Model model,@ModelAttribute("assetParkingLotULB")AssetParkingLotULB asParkingLotULB){
	
		
		model.addAttribute("assetParkingLotULB" ,asParkingLotULB);   
		model.addAttribute("zones",this.zoneService.listZones());
		parkingService.addAssetParkingLotULB(asParkingLotULB);
		/*if(validation.isValidData(model, asParkingLotULB)){
			return "asset_parking_lot_ULB";
		}*/
		
        return "redirect:/parking/asset_parkingLotULB";
    }
	
	   
	
	
	@RequestMapping(value = "/asset_parkingLot_auctioned" , method = RequestMethod.GET)
    public String assetParkingLotAuctioned(Model model){
       model.addAttribute("assetParkingLotAuctioned" ,new AssetParkingLotAuctioned());
       model.addAttribute("zones",this.zoneService.listZones());  
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
        return "asset_parkingLot_auctioned";
    }
	
	
	@RequestMapping(value = "/asset_parkingLot_auctioned" , method = RequestMethod.POST)
    public String addAssetParkingLotAuctioned(Model model,@ModelAttribute("assetParkingLotAuctioned")AssetParkingLotAuctioned assetParkingLotAuctioned){
	
		parkingService.addAssetParkingLotAuctioned(assetParkingLotAuctioned);
		/*if(validation.isValidData(model, assetParkingLotAuctioned)){
			return "asset_parkingLot_auctioned";
		}*/
        return "redirect:/parking/asset_parkingLot_auctioned";
    }
	
	
	@RequestMapping(value = "/big_defaulters" , method = RequestMethod.GET)  
    public String bigDefaulters(Model model){
		model.addAttribute("bigDefaulters" ,new BigDefaultersModel());
		model.addAttribute("financialYears", this.financialYearService.listFinancialYears());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		model.addAttribute("wards",this.wardService.listWards());
	
        return "big_defaulters";
    }
	
	
	@RequestMapping(value = "/big_defaulters" , method = RequestMethod.POST)  
    public String addBigDefaulters(Model model,@ModelAttribute("bigDefaulters")BigDefaultersModel bDefaultersModel){
	

		model.addAttribute("bigDefaulters" ,bDefaultersModel);
		
		/*if(validation.isValidData(model, bDefaultersModel)){
			return "big_defaulters";
		}*/
		
        return "redirect:/parking/big_defaulters";
    }
	  
	
	
	@RequestMapping(value = "/tax_rates" , method = RequestMethod.GET)
    public String taxRates(Model model){
		model.addAttribute("taxRate" ,new TaxRates());
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
        return "tax_rates";
    }
	
	
	@RequestMapping(value = "/tax_rates" , method = RequestMethod.POST)
    public String addTaxRates(Model model,@ModelAttribute("taxRate")TaxRates taxrate){
		model.addAttribute("taxRate" ,taxrate);
		model.addAttribute("listDistricts",this.districtService.listDistricts());
		model.addAttribute("ulbs",this.ulbService.listUlbs());
		
		
		/*if(validation.isValidData(model, taxrate)){
			return "tax_rates";
		}*/
        return "redirect:/parking/tax_rates";
    }
	
	
	
	
	
	
	
	
	
	
}
