package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="parking_asset_auction")  
public class AssetParkingLotAuctioned {  

	  
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Transient
	private District  district;
	
	@Transient
	private Ulb  ulb;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward  ward;
	
	@ManyToOne
	@JoinColumn(name="zone_id")
	private Zone  zone;
	
	@Column(name="location")
	private String  Location;

	@Column(name="area")
	private float  totalArea;
	
	@Column(name="UPF_price")
	private int  upsetPrice;   
	
	@Column(name="name_of_allotee")
	private String  nameOfAllotment;
	
	@Column(name="contract_amount")
	private double  contractAmount;
	
	@Column(name="contract_start_date")
	private Date  contractValidFrom;
	
	@Column(name="contract_end_date")
	private Date  contractValidTill;
	
	@Transient
	private int  contractDuration ;
	
	@Column(name="created_at")
	private Date createdAt= new Date();
	
	@Column(name="updated_at")
	private Date updatedAt=new Date();
	
	
	
	@Column(name="status")
	private int  status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public float getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(float totalArea) {
		this.totalArea = totalArea;
	}

	public int getUpsetPrice() {
		return upsetPrice;
	}

	public void setUpsetPrice(int upsetPrice) {
		this.upsetPrice = upsetPrice;
	}

	public String getNameOfAllotment() {
		return nameOfAllotment;
	}

	public void setNameOfAllotment(String nameOfAllotment) {
		this.nameOfAllotment = nameOfAllotment;
	}

	public double getContractAmount() {
		return contractAmount;
	}

	public void setContractAmount(double contractAmount) {
		this.contractAmount = contractAmount;
	}

	public Date getContractValidFrom() {
		return contractValidFrom;
	}

	public void setContractValidFrom(Date contractValidFrom) {
		this.contractValidFrom = contractValidFrom;
	}

	public Date getContractValidTill() {
		return contractValidTill;
	}

	public void setContractValidTill(Date contractValidTill) {
		this.contractValidTill = contractValidTill;
	}

	public int getContractDuration() {
		return contractDuration;
	}

	public void setContractDuration(int contractDuration) {
		this.contractDuration = contractDuration;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
}