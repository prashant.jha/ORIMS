package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


public class TaxRates {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private District  district;
	private Ulb  ulb;
	
	private String  holdingTax;
	private Date  lastModified;
	
	
	@Column(name="created_at")
	private Date createdAt = new Date();
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();
	
	
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public Ulb getUlb() {
		return ulb;
	}
	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}
	public String getHoldingTax() {
		return holdingTax;
	}
	public void setHoldingTax(String holdingTax) {
		this.holdingTax = holdingTax;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	
	
	
	
}