package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="trade_licence_number")
public class TradeLicence {


	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="financial_year")
	private FinancialYear financialYear;
	
	@Transient
	private District  district;
	
	@ManyToOne
	@JoinColumn(name="ulb_id")
	private Ulb  ulb;
	  
	@Column(name="new_licence")
	private double  newLicence ;
	
	@Column(name="renewal")
	private double  renewal ;
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();
	
	@Column(name="created_at")
	private Date createdAt = new Date();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public double getNewLicence() {
		return newLicence;
	}

	public void setNewLicence(double newLicence) {
		this.newLicence = newLicence;
	}

	public double getRenewal() {
		return renewal;
	}

	public void setRenewal(double renewal) {
		this.renewal = renewal;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
}