package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="collection_advertisement_taxes")
public class CollectionAdvertismentTaxes {

	@Id
	@Column(name="id")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="financial_year_id")
	private FinancialYear financialYear;
	
	@ManyToOne
	@JoinColumn(name="advertisement_type_id")
	private AdvertismentTypes advertismentTypes;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward ward;
	
	@ManyToOne
	@JoinColumn(name="ulb_id")
	private Ulb ulb;
	
	@Column(name="amount")
	private float amount;
	
	@Column(name="collection_date")
	@Temporal(TemporalType.DATE)
	private Date collectionDate;

	@Column(name="comments")
	private String comments;
	
	@Column(name="is_deleted")
	private Boolean isDeleted;
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();
	
	@Column(name="created_at")
	private Date createdAt = new Date();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public AdvertismentTypes getAdvertismentTypes() {
		return advertismentTypes;
	}

	public void setAdvertismentTypes(AdvertismentTypes advertismentTypes) {
		this.advertismentTypes = advertismentTypes;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public float getAmount() {
		return amount;
	}

	public void setAmount(float amount) {
		this.amount = amount;
	}

	public Date getCollectionDate() {
		return collectionDate;
	}

	public void setCollectionDate(Date collectionDate) {
		this.collectionDate = collectionDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	
}
