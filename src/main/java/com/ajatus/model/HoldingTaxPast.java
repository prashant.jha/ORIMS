package com.ajatus.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="holding_tax_past")
public class HoldingTaxPast {
	
	@Id
	@Column(name="id")
	private int id;  
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="financial_year")
	private FinancialYear financialYear;
	
	@Transient
	private District district;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ulb_id")
	private Ulb ulb;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "month_id")
	private Month month;
	
	@Column(name="current_collection")
	private double currentCollection; 
	
	@Column(name="arrear_collection")
	private double arrear;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(Month month) {
		this.month = month;
	}

	public double getCurrentCollection() {
		return currentCollection;
	}

	public void setCurrentCollection(double currentCollection) {
		this.currentCollection = currentCollection;
	}

	public double getArrear() {
		return arrear;
	}

	public void setArrear(double arrear) {
		this.arrear = arrear;
	} 

}
