package com.ajatus.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;
import com.ajatus.model.Ulb;
import com.ajatus.model.Ward;

@Entity
@Table(name="prking")
public class ParkingLots  {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="financial_year")
	private FinancialYear financialYear;
	
	@Transient
	private District  district;
	
	@Transient
	private Ulb  ulb;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward  ward;
	
	@ManyToOne
	@JoinColumn(name="zone_id")
	private Zone  zone;
	
	@Column(name="type")
	private String  parkingManagedType;
	
	@Column(name="date")
	private Date  date;     
	
	@ManyToOne
	@JoinColumn(name="fee_collector_id")
	private FeeCollector  feeCollector ;
	
	@Column(name="collection_amount")
	private int  collectionAmount;
	
	@Column(name="location")
	private String  location;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@Column(name="updated_at")
	private Date updatedAt;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public String getParkingManagedType() {
		return parkingManagedType;
	}

	public void setParkingManagedType(String parkingManagedType) {
		this.parkingManagedType = parkingManagedType;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	
	
	
	

	public FeeCollector getFeeCollector() {
		return feeCollector;
	}

	public void setFeeCollector(FeeCollector feeCollector) {
		this.feeCollector = feeCollector;
	}

	public int getCollectionAmount() {
		return collectionAmount;
	}

	public void setCollectionAmount(int collectionAmount) {
		this.collectionAmount = collectionAmount;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}
	
	
	
	
}