package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;
import com.ajatus.model.Ulb;


@Entity
@Table(name="parking_past")
public class ParkingLotsPast {
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="financial_year")
	private FinancialYear financialYear;
	
	@Transient
	private District district;
	
	@ManyToOne
	@JoinColumn(name="ulb")
	private Ulb ulb;
	
	@Column(name="area")
	private double parkingArea;
	
	@Column(name="no_of_lots")
	private int noOfParkingLots;
	
	@Column(name="collection_amount")
	private double collectionAmount;
	
	
	@Column(name="created_at")
	private Date createdAt = new Date();;
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();;
	
	@Column(name="status")
	private int status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public double getParkingArea() {
		return parkingArea;
	}

	public void setParkingArea(double parkingArea) {
		this.parkingArea = parkingArea;
	}

	public int getNoOfParkingLots() {
		return noOfParkingLots;
	}

	public void setNoOfParkingLots(int noOfParkingLots) {
		this.noOfParkingLots = noOfParkingLots;
	}

	public double getCollectionAmount() {
		return collectionAmount;
	}

	public void setCollectionAmount(double collectionAmount) {
		this.collectionAmount = collectionAmount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
	
	

}
