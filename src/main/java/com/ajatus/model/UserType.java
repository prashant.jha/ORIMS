package com.ajatus.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.IndexColumn;

@Entity
@Table(name="user_types")
public class UserType {

//	private Set<Users> Users;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="executing_authority")
	private String executingAuthority;
	
	@Column(name="created_at")
	private Date createdAt;
	
	@Column(name="updated_at")
	private Date updatedAt;
	
	/**
	 * @return the updatedAt
	 */
	public Date getUpdatedAt() {
		return updatedAt;
	}



	/**
	 * @param updatedAt the updatedAt to set
	 */
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}



	@Column(name="status")
	private Boolean status;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}



	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}



	/**
	 * @return the executingAuthority
	 */
	public String getExecutingAuthority() {
		return executingAuthority;
	}



	/**
	 * @param executingAuthority the executingAuthority to set
	 */
	public void setExecutingAuthority(String executingAuthority) {
		this.executingAuthority = executingAuthority;
	}



	/**
	 * @return the createdAt
	 */
	public Date getCreatedAt() {
		return createdAt;
	}



	/**
	 * @param createdAt the createdAt to set
	 */
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}



	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}



	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}



	@Override
	public String toString(){
		return "id="+id+",executingAuthority ="+executingAuthority;
	}
	
	@OneToMany(mappedBy="userType", cascade = CascadeType.ALL)
	private Set<User> users;

	public Set<User> getUsers() {
		return users;
	}



	public void setUsers(Set<User> users) {
		this.users = users;
	}
	
//	 @OneToMany(mappedBy = "UsersType", cascade = CascadeType.ALL)
//	    public Set<Users> getUsers() {
//	        return Users;
//	    }
//
//	    public void setUsers(Set<Users> Users) {
//	        this.Users = Users;
//	    }
}
