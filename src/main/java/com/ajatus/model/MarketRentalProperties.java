package com.ajatus.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="market_rental_properties")
public class MarketRentalProperties {
	
	@Id
	@Column(name="id")
	private int id;
	@Transient
	private District district;
	@ManyToOne
	@JoinColumn(name="ulb_id")
	private Ulb ulb;
	
	@Column(name="type")
	private String types;
	
	@Column(name="numbers")
	private int numbers;
	
	@Column(name="updates_at")
	private Date updatedAt = new Date();
	
	@Column(name="created_at")
	private Date createdAt = new Date();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

         
	public String getTypes() {
		return types;
	}

	public void setTypes(String types) {
		this.types = types;
	}

	public int getNumbers() {
		return numbers;
	}

	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	
}
