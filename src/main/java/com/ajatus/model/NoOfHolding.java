package com.ajatus.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;

@Entity
@Table(name="no_of_holdings")
public class NoOfHolding {


	@Id
	@Column(name="id")
	private int id;  
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="financial_year")
	private FinancialYear financialYear;
	
	@Transient
	private District district;
	
	@Transient
	private Ulb ulb;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="ward_id")
	private Ward ward;
	
	@Column(name="no_of_holding")
	@Min(
            value = 1,
            message = "This field should not have 0"
    ) 
	private int noOfHoldings;
	
	@Column(name="no_of_new_commercial_holdings")
	private int commercialHoldings;
	
	@Column(name="no_of_new_residential_holdings")   
	private int residentalHoldings;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public int getNoOfHoldings() {
		return noOfHoldings;
	}

	public void setNoOfHoldings(int noOfHoldings) {
		this.noOfHoldings = noOfHoldings;
	}

	public int getCommercialHoldings() {
		return commercialHoldings;
	}

	public void setCommercialHoldings(int commercialHoldings) {
		this.commercialHoldings = commercialHoldings;
	}

	public int getResidentalHoldings() {
		return residentalHoldings;
	}

	public void setResidentalHoldings(int residentalHoldings) {
		this.residentalHoldings = residentalHoldings;
	}
	
	
}
