package com.ajatus.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name="demand_wards")
public class DemandWard {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="yearly_demand_amount")
	private float yearlyDemandAmount;
	
	@Column(name="is_set_monthly")
	private boolean isSetMonthly;
	
	@OneToMany(mappedBy ="demandWard",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	private List<DemandWardMonth> demandWardMonths;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="financial_year_id")
	private FinancialYear financialYear;
	
	@ManyToOne
	@JoinColumn(name="district_id")
	private District district;
	
	@ManyToOne
	@JoinColumn(name="ulb_id")
	private Ulb ulb;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward ward;
	
	@Column(name="created_at")
	private Date createdAt = new Date();
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getYearlyDemandAmount() {
		return yearlyDemandAmount;
	}

	public void setYearlyDemandAmount(float yearlyDemandAmount) {
		this.yearlyDemandAmount = yearlyDemandAmount;
	}

	public boolean getIsSetMonthly() {
		return isSetMonthly;
	}

	public void setIsSetMonthly(boolean isSetMonthly) {
		this.isSetMonthly = isSetMonthly;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@PreUpdate
	public void setUpdatedAt() {
		this.updatedAt = new Date();
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public List<DemandWardMonth> getDemandWardMonths() {
		return demandWardMonths;
	}

	public void setDemandWardMonths(List<DemandWardMonth> demandWardMonths) {
		for (DemandWardMonth demandWardMonth : demandWardMonths) {
			demandWardMonth.setDemandWard(this);
		}
		this.demandWardMonths = demandWardMonths;
	}

	
}
