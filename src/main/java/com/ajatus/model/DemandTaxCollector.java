package com.ajatus.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name="demand_tax_collectors")
public class DemandTaxCollector {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="yearly_demand_amount")
	private float yearlyDemandAmount;
	
	@Column(name="is_set_monthly")
	private boolean isSetMonthly;
	
	@OneToMany(mappedBy ="demandTaxCollector",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
	private List<DemandTaxCollectorMonth> demandTaxCollectorMonths;
	
	@ManyToOne
	@JoinColumn(name="tax_collector_id")
	private TaxCollectorModel taxCollector;
	
	@ManyToOne
	@JoinColumn(name="demand_ward_id")
	private DemandWard demandWard;
	
	@Column(name="created_at")
	private Date createdAt = new Date();
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();
	
	@Column(name="is_deleted")
	private Boolean isDeleted;

	public int getId() {
		return id;
	}

	public float getYearlyDemandAmount() {
		return yearlyDemandAmount;
	}

	public void setYearlyDemandAmount(float yearlyDemandAmount) {
		this.yearlyDemandAmount = yearlyDemandAmount;
	}

	public boolean getIsSetMonthly() {
		return isSetMonthly;
	}

	public void setIsSetMonthly(boolean isSetMonthly) {
		this.isSetMonthly = isSetMonthly;
	}

	public TaxCollectorModel getTaxCollector() {
		return taxCollector;
	}

	public void setTaxCollector(TaxCollectorModel taxCollector) {
		this.taxCollector = taxCollector;
	}

	public DemandWard getDemandWard() {
		return demandWard;
	}

	public void setDemandWard(DemandWard demandWard) {
		this.demandWard = demandWard;
	}

	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@PreUpdate
	public void setUpdatedAt() {
		this.updatedAt = new Date();
	}

	public List<DemandTaxCollectorMonth> getDemandTaxCollectorMonths() {
		return demandTaxCollectorMonths;
	}

	public void setDemandTaxCollectorMonths(List<DemandTaxCollectorMonth> demandTaxCollectorMonths) {
		for (DemandTaxCollectorMonth demandTaxCollectorMonth : demandTaxCollectorMonths) {
			demandTaxCollectorMonth.setDemandTaxCollector(this);
		}
		this.demandTaxCollectorMonths = demandTaxCollectorMonths;
	}

	
}
