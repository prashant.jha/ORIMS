package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.ajatus.model.District;
import com.ajatus.model.FinancialYear;
import com.ajatus.model.Ulb;
import com.ajatus.model.Ward;

public class BigDefaultersModel   {

	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	
	private FinancialYear financialYear;
	private District  district;
	private Ulb  ulb;
	private Ward  ward;
	private String  taxCollector;
	private String  address;
	private int  holding;
	private int  totalOutstanding;
	private Date  periodFrom;
	private Date  periodTo;
	private int  totalDuration;
	
	
	@Column(name="created_at")
	private Date createdAt= new Date();
	
	@Column(name="updated_at")
	private Date updatedAt=new Date();
	
	
	
	public FinancialYear getFinancialYear() {
		return financialYear;
	}
	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}
	public District getDistrict() {
		return district;
	}
	public void setDistrict(District district) {
		this.district = district;
	}
	public Ulb getUlb() {
		return ulb;
	}
	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}
	public Ward getWard() {
		return ward;
	}
	public void setWard(Ward ward) {
		this.ward = ward;
	}
	public String getTaxCollector() {
		return taxCollector;
	}
	public void setTaxCollector(String taxCollector) {
		this.taxCollector = taxCollector;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getHolding() {
		return holding;
	}
	public void setHolding(int holding) {
		this.holding = holding;
	}
	public int getTotalOutstanding() {
		return totalOutstanding;
	}
	public void setTotalOutstanding(int totalOutstanding) {
		this.totalOutstanding = totalOutstanding;
	}
	public Date getPeriodFrom() {
		return periodFrom;
	}
	public void setPeriodFrom(Date periodFrom) {
		this.periodFrom = periodFrom;
	}
	public Date getPeriodTo() {
		return periodTo;
	}
	public void setPeriodTo(Date periodTo) {
		this.periodTo = periodTo;
	}
	public int getTotalDuration() {
		return totalDuration;
	}
	public void setTotalDuration(int totalDuration) {
		this.totalDuration = totalDuration;
	}
	
	
	
	
	
}
