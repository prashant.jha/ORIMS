package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="advertisement_types")
public class AdvertismentTypes {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id")
private int id;

@Column(name="name")
private String name;

@Column(name="updated_at")
private Date updatedAt = new Date();

@Column(name="is_deleted")
private Boolean isDeleted;

@Column(name="created_at")
private Date createdAt = new Date();

//----------------------------------Getters and Setters---------------------------------------------
		public Date getCreatedAt() {
			return createdAt;
		}
		
		public Date getUpdatedAt() {
			return updatedAt;
		}

		public void setUpdatedAt() {
			this.updatedAt = new Date();
		}	

		public Boolean getIsDeleted() {
			return isDeleted;
		}

		public void setIsDeleted(Boolean isDeleted) {
			this.isDeleted = isDeleted;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

}
