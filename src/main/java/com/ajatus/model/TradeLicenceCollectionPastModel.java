package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="trade_licence_collection_past")
public class TradeLicenceCollectionPastModel {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="financial_year")
	private FinancialYear financialYear;
	
	@Transient
	private District  district;
	@Transient
	private Ulb  ulb;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward  ward;

	@Column(name="collection_amount_type")
	private String collectionAmountType;

	@Column(name="new_licence")
	private double  newLicence;

	@Column(name="renewal")
	private double  renewal;
	
	@Column(name="aggregate")
	private double  aggregate;
	

	@Column(name="created_at")
	private Date createdAt = new Date();
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public String getCollectionAmountType() {
		return collectionAmountType;
	}

	public void setCollectionAmountType(String collectionAmountType) {
		this.collectionAmountType = collectionAmountType;
	}

	public double getNewLicence() {
		return newLicence;
	}

	public void setNewLicence(double newLicence) {
		this.newLicence = newLicence;
	}

	public double getRenewal() {
		return renewal;
	}

	public void setRenewal(double renewal) {
		this.renewal = renewal;
	}

	public double getAggregate() {
		return aggregate;
	}

	public void setAggregate(double aggregate) {
		this.aggregate = aggregate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
	
	
}
