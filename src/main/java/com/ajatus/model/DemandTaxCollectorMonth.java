package com.ajatus.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

@Entity
@Table(name="demand_tax_collector_months")
public class DemandTaxCollectorMonth {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="monthly_demand_amount")
	private float monthlyDemandAmount;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "month_id", nullable = false)
	private Month month;
	
	@ManyToOne(fetch = FetchType.EAGER,cascade= CascadeType.ALL)
	@JoinColumn(name = "demand_tax_collector_id", nullable = false)
	private DemandTaxCollector demandTaxCollector;
	
	@Column(name="created_at")
	private Date createdAt = new Date();
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();

	public int getId() {
		return id;
	}

	public float getMonthlyDemandAmount() {
		return monthlyDemandAmount;
	}

	public void setMonthlyDemandAmount(float monthlyDemandAmount) {
		this.monthlyDemandAmount = monthlyDemandAmount;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(Month month) {
		this.month = month;
	}

	public DemandTaxCollector getDemandTaxCollector() {
		return demandTaxCollector;
	}

	public void setDemandTaxCollector(DemandTaxCollector demandTaxCollector) {
		this.demandTaxCollector = demandTaxCollector;
	}
	
	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@PreUpdate
	public void setUpdatedAt() {
		this.updatedAt = new Date();
	}
}
