package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="parking_asset")  
public class AssetParkingLotULB {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Transient
	private District  district;
	
	@Transient
	private Ulb  ulb;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward  ward;
	
	@ManyToOne
	@JoinColumn(name="zone_id")
	private Zone  zone;
	
	@Column(name="location")
	private String  Location;
	
	@Column(name="area")
	private int  totalArea;
	
	@Column(name="no_of_feecollectors")
	private int  noOfCollectors;
	
	@Column(name="Name_feecollector")
	private String  nameFeeCollector;
	
	@Column(name="handheld_machin")
	private String  machineAvailable;
	
	@Column(name="is_auctuned")
	private String  auctionedEarlier ;
	
	@Column(name="LDC")
	private Date  LastDateOfContract;
	
	@Column(name="LDU_price")
	private double  lastUpsetPrice;
	
	@Column(name="created_at")
	private Date createdAt= new Date();
	
	@Column(name="updated_at")
	private Date updatedAt=new Date();
	
	@Column(name="status")
	private int  status;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public District getDistrict() {
		return district;
	}

	public void setDistrict(District district) {
		this.district = district;
	}

	public Ulb getUlb() {
		return ulb;
	}

	public void setUlb(Ulb ulb) {
		this.ulb = ulb;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public Zone getZone() {
		return zone;
	}

	public void setZone(Zone zone) {
		this.zone = zone;
	}

	public String getLocation() {
		return Location;
	}

	public void setLocation(String location) {
		Location = location;
	}

	public int getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(int totalArea) {
		this.totalArea = totalArea;
	}

	public int getNoOfCollectors() {
		return noOfCollectors;
	}

	public void setNoOfCollectors(int noOfCollectors) {
		this.noOfCollectors = noOfCollectors;
	}

	public String getNameFeeCollector() {
		return nameFeeCollector;
	}

	public void setNameFeeCollector(String nameFeeCollector) {
		this.nameFeeCollector = nameFeeCollector;
	}

	public String getMachineAvailable() {
		return machineAvailable;
	}

	public void setMachineAvailable(String machineAvailable) {
		this.machineAvailable = machineAvailable;
	}

	public String getAuctionedEarlier() {
		return auctionedEarlier;
	}

	public void setAuctionedEarlier(String auctionedEarlier) {
		this.auctionedEarlier = auctionedEarlier;
	}

	public Date getLastDateOfContract() {
		return LastDateOfContract;
	}

	public void setLastDateOfContract(Date lastDateOfContract) {
		LastDateOfContract = lastDateOfContract;
	}

	public double getLastUpsetPrice() {
		return lastUpsetPrice;
	}

	public void setLastUpsetPrice(double lastUpsetPrice) {
		this.lastUpsetPrice = lastUpsetPrice;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	
	
}