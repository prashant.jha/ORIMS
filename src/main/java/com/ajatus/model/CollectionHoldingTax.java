package com.ajatus.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="collection_holding_taxes")
public class CollectionHoldingTax {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	@JoinColumn(name="financial_year_id")
	private FinancialYear financialYear;
	
	@Column(name="property_type")
	private String propertyType;
	
	@Column(name="collection_mode")
	private String collectionMode;
	
	@ManyToOne
	@JoinColumn(name="ward_id")
	private Ward ward;
	
	@ManyToOne
	@JoinColumn(name="ward_office_id")
	private WardOffice wardOffice;
	
	@ManyToOne
	@JoinColumn(name="tax_collector_id")
	private Ward taxCollector;
	
	@Column(name="current_amount")
	private float currentAmount;
	
	@Column(name="arrear_amount")
	private float arrearAmount;
	
	@Column(name="no_of_holdings")
	private int noOfHoldings;
	
	@Column(name="collection_date")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private Date collectionDate;
	
	@Column(name="created_at")
	private Date createdAt = new Date();
	
	@Column(name="updated_at")
	private Date updatedAt = new Date();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FinancialYear getFinancialYear() {
		return financialYear;
	}

	public void setFinancialYear(FinancialYear financialYear) {
		this.financialYear = financialYear;
	}

	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	public String getCollectionMode() {
		return collectionMode;
	}

	public void setCollectionMode(String collectionMode) {
		this.collectionMode = collectionMode;
	}

	public Ward getWard() {
		return ward;
	}

	public void setWard(Ward ward) {
		this.ward = ward;
	}

	public WardOffice getWardOffice() {
		return wardOffice;
	}

	public void setWardOffice(WardOffice wardOffice) {
		this.wardOffice = wardOffice;
	}

	public Ward getTaxCollector() {
		return taxCollector;
	}

	public void setTaxCollector(Ward taxCollector) {
		this.taxCollector = taxCollector;
	}

	public float getCurrentAmount() {
		return currentAmount;
	}

	public void setCurrentAmount(float currentAmount) {
		this.currentAmount = currentAmount;
	}

	public float getArrearAmount() {
		return arrearAmount;
	}

	public void setArrearAmount(float arrearAmount) {
		this.arrearAmount = arrearAmount;
	}

	public int getNoOfHoldings() {
		return noOfHoldings;
	}

	public void setNoOfHoldings(int noOfHoldings) {
		this.noOfHoldings = noOfHoldings;
	}

	public Date getCollectionDate() {
		return collectionDate;
	}

	public void setCollectionDate(Date collectionDate) {
		this.collectionDate = collectionDate;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}
	
	@PreUpdate
	public void setUpdatedAt() {
		this.updatedAt = new Date();
	}
	
}
