package com.ajatus.jpa;

import com.ajatus.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface  UserJPA extends JpaRepository<User, Long> {
	public User findByUserName(String userName);
}
