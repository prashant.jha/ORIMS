package com.ajatus.jpa;

import com.ajatus.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserTypeJPA extends JpaRepository<UserType, Long>{
	
}


