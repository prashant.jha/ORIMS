package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.WardDAO;
import com.ajatus.model.Ward;
@Service
public class WardServiceImpl implements WardService {

	private WardDAO WardDAO;

	public void setWardDAO(WardDAO WardDAO) {
		this.WardDAO = WardDAO;
	}
	
	@Override
	@Transactional
	public void addWard(Ward w) {
		this.WardDAO.addWard(w);
	}

	@Override
	@Transactional
	public void updateWard(Ward w) {
		this.WardDAO.updateWard(w);
	}

	@Override
	@Transactional
	public List<Ward> listWards() {
		return this.WardDAO.listWards();
	}

	@Override
	@Transactional
	public Ward getWardById(int id) {
		return this.WardDAO.getWardById(id);
	}

	@Override
	@Transactional
	public void removeWard(int id) {
		this.WardDAO.removeWard(id);
		
	}

	@Override
	@Transactional
	public List<Ward> listWardsByUlb(int ulb_id) {
		return this.WardDAO.listWardsByUlb(ulb_id);
	}

}
