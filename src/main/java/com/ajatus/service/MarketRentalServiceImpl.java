package com.ajatus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.MarketRentalDAO;
import com.ajatus.model.MarketRentalCollection;
import com.ajatus.model.MarketRentalCollectionPast;
import com.ajatus.model.MarketRentalProperties;
@Service
public class MarketRentalServiceImpl implements MarketRentalService {

	@Autowired(required=true)
	private MarketRentalDAO marketRentalDAO;
	
	
	@Override
	@Transactional
	public void addMarketRentalProperties(MarketRentalProperties mrp) {
		
		this.marketRentalDAO.addMarketRentalProperties(mrp);
	}

	@Override
	public void updateMarketRentalProperties(MarketRentalProperties mrp) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMarketRentalProperties(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MarketRentalProperties> listMarketRentalProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MarketRentalProperties listMarketRentalPropertiesById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	@Override
	@Transactional
	public void addMarketRentalCollection(MarketRentalCollection marketRentalCollection) {
		
		this.marketRentalDAO.addMarketRentalCollection(marketRentalCollection);
		
	}

	@Override
	public void updateMarketRentalCollection(MarketRentalCollection marketRentalCollection) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMarketRentalCollection(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MarketRentalCollection> listMarketRentalCollection() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MarketRentalCollection listMarketRentalCollectionById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	



	@Override
	@Transactional
	public void addMarketRentalCollectionPast(MarketRentalCollectionPast marketRentalCollectionPast) {
		this.marketRentalDAO.addMarketRentalCollectionPast(marketRentalCollectionPast);
		
	}

	@Override
	public void updateMarketRentalCollectionPast(MarketRentalCollectionPast marketRentalCollectionPast) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeMarketRentalCollectionPast(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<MarketRentalCollectionPast> listMarketRentalCollectionPast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MarketRentalCollectionPast listMarketRentalCollectionPastById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
