package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.UlbDAO;
import com.ajatus.model.Ulb;
@Service
public class UlbServiceImpl implements UlbService {

	private UlbDAO UlbDAO;

	public void setUlbDAO(UlbDAO UlbDAO) {
		this.UlbDAO = UlbDAO;
	}
	
	@Override
	@Transactional
	public void addUlb(Ulb u) {
		this.UlbDAO.addUlb(u);
		
	}

	@Override
	@Transactional
	public void updateUlb(Ulb u) {
		this.UlbDAO.updateUlb(u);
		
	}

	@Override
	@Transactional
	public List<Ulb> listUlbs() {
		
		return this.UlbDAO.listUlbs();
	}

	@Override
	@Transactional
	public Ulb getUlbById(int id) {
		return this.UlbDAO.getUlbById(id);
	}

	@Override
	@Transactional
	public void removeUlb(int id) {
		this.UlbDAO.removeUlb(id);
		
	}

	@Override
	@Transactional
	public List<Ulb> listUlbsByDistrict(int district_id) {
		// TODO Auto-generated method stub
		return this.UlbDAO.listUlbsByDistrict(district_id);
	}

}
