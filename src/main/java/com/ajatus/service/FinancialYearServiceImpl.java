package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.FinancialYearDAO;
import com.ajatus.model.FinancialYear;

@Service
public class FinancialYearServiceImpl implements FinancialYearService {

	private FinancialYearDAO financialYearDAO;
	
	public void setFinancialYearDAO(FinancialYearDAO financialYearDAO) {
		this.financialYearDAO = financialYearDAO;
	}
	
	@Override
	@Transactional
	public void addFinancialYear(FinancialYear f) {
		this.financialYearDAO.addFinancialYear(f);
		
	}

	@Override
	@Transactional
	public void updateFinancialYear(FinancialYear f) {
		this.financialYearDAO.updateFinancialYear(f);
		
	}

	@Override
	@Transactional
	public List<FinancialYear> listFinancialYears() {
		return this.financialYearDAO.listFinancialYears();
	}

	@Override
	@Transactional
	public FinancialYear getFinancialYearById(int id) {
		return this.financialYearDAO.getFinancialYearById(id);
	}

	@Override
	@Transactional
	public void removeFinancialYear(int id) {
		this.financialYearDAO.removeFinancialYear(id);
		
	}

}
