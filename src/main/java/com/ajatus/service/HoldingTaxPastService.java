package com.ajatus.service;

import java.util.List;

import com.ajatus.model.HoldingTaxPast;

public interface HoldingTaxPastService {

	public void addHoldingTaxPast(HoldingTaxPast holdingTaxPast);
	public void updateHoldingTaxPast(HoldingTaxPast holdingTaxPast);
	public void removeHoldingTaxPast(int id);
	public List<HoldingTaxPast> listHoldingTaxPast();
	public HoldingTaxPast listHoldingTaxPastById(int id);
}
