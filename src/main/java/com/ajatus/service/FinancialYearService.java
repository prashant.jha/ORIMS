package com.ajatus.service;

import java.util.List;

import com.ajatus.model.FinancialYear;

public interface FinancialYearService {
	
	public void addFinancialYear(FinancialYear f);
	public void updateFinancialYear(FinancialYear f);
	public List<FinancialYear> listFinancialYears();
	public FinancialYear getFinancialYearById(int id);
	public void removeFinancialYear(int id);
}
