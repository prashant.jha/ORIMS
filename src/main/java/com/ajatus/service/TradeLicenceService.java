package com.ajatus.service;

import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.model.TradeLicenceCollectionModel;
import com.ajatus.model.TradeLicenceCollectionPastModel;
import com.ajatus.model.TradeLicenceDemandModel;

public interface TradeLicenceService {

	void addTradeLicenceDemand(TradeLicenceDemandModel tradeLicenceDemandModel);

	void addTradeLicenceCollection(TradeLicenceCollectionModel tradeLicenceCollectionModel);

	void addTradeLicenceCollectionPast(TradeLicenceCollectionPastModel tradeLicenceCollectionPastModel);

	void addNoTradeLicence(TradeLicence tLicence);

	void addTradeBigDefaulters(TradeBigDefaultersModel tradeBigDefaultersModel);

}
