package com.ajatus.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.FeeCollectorDAO;
import com.ajatus.model.FeeCollector;

@Service
public class FeeCollectorServiceImpl implements FeeCollectorService{

	@Autowired(required=true)
	private FeeCollectorDAO feeCollectorDAO;
	
	@Override
	@Transactional
	public void addFeeCollectorDAO(FeeCollector d) {
	
		this.feeCollectorDAO.addFeeCollectorDAO(d);
		
	}

	@Override
	@Transactional
	public void updateFeeCollector(FeeCollector d) {
		this.feeCollectorDAO.updateFeeCollector(d);
		
	}

	@Override
	@Transactional
	public List<FeeCollector> listFeeCollector() {
		List<FeeCollector> feeCollectors =new ArrayList<FeeCollector>(); ;
	 try {
		feeCollectors =  this.feeCollectorDAO.listFeeCollector();
	} catch (Exception e) {
		FeeCollector f = new FeeCollector();
		f.setId(1);
		f.setName("Manas");
		feeCollectors.add(f);
		e.printStackTrace();
	}
		return feeCollectors;
	}

	@Override
	@Transactional
	public FeeCollector getFeeCollectorById(int id) {
		// TODO Auto-generated method stub
		return this.feeCollectorDAO.getFeeCollectorById(id);
	}

	@Override
	@Transactional
	public void removeFeeCollector(int id) {
		this.feeCollectorDAO.removeFeeCollector(id);
		
	}
	

}
