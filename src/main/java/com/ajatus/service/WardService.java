package com.ajatus.service;

import java.util.List;

import com.ajatus.model.Ward;

public interface WardService {

	public void addWard(Ward w);
	public void updateWard(Ward w);
	public List<Ward> listWards();
	public Ward getWardById(int id);
	public void removeWard(int id);
	public List<Ward> listWardsByUlb(int ulb_id);
}
