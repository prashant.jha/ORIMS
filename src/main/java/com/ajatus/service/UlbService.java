package com.ajatus.service;

import java.util.List;

import com.ajatus.model.Ulb;

public interface UlbService {
	
	public void addUlb(Ulb u);
	public void updateUlb(Ulb u);
	public List<Ulb> listUlbs();
	public Ulb getUlbById(int id);
	public void removeUlb(int id);
	public List<Ulb> listUlbsByDistrict(int district_id);
}
