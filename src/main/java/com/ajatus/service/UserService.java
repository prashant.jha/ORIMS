package com.ajatus.service;

import java.util.List;

import com.ajatus.model.User;


public interface UserService {

	public void addUser(User p);
	public void updateUser(User p);
	public List<User> listUsers();
	public User getUserById(int id);
	public void removeUser(int id);
	
	void save(User user);

    User findByUsername(String username);
}
