package com.ajatus.service;

import java.util.List;

import com.ajatus.model.FeeCollector;

public interface FeeCollectorService {
	public void addFeeCollectorDAO(FeeCollector d);
	public void updateFeeCollector(FeeCollector d);
	public List<FeeCollector> listFeeCollector();
	public FeeCollector getFeeCollectorById(int id);
	public void removeFeeCollector(int id);

}
