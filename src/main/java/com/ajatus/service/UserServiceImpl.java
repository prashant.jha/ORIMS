package com.ajatus.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ajatus.dao.UserDAO;
import com.ajatus.jpa.UserJPA;
import com.ajatus.model.User;


@Service
public class UserServiceImpl implements UserService {
	
	private UserDAO UserDAO;
	@Autowired
	private UserJPA UserJPA;

	public void setUserDAO(UserDAO UserDAO) {
		this.UserDAO = UserDAO;
	}
	
	public void setUserJPA(UserJPA UserJPA) {
		this.UserJPA = UserJPA;
	}

	@Override
	@Transactional
	public void addUser(User p) {
		this.UserDAO.addUser(p);
	}

	@Override
	@Transactional
	public void updateUser(User p) {
		this.UserDAO.updateUser(p);
	}

	@Override
	@Transactional
	public List<User> listUsers() {
		return this.UserDAO.listUsers();
	}

	@Override
	@Transactional
	public User getUserById(int id) {
		return this.UserDAO.getUserById(id);
	}

	@Override
	@Transactional
	public void removeUser(int id) {
		this.UserDAO.removeUser(id);
	}

	public User findByUsername(String username) {
		return this.UserJPA.findByUserName(username);
	}

	@Override
	public void save(User user) {
		// TODO Auto-generated method stub
		
	}

}
