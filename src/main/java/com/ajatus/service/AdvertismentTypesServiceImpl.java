package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.AdvertismentTypesDAO;
import com.ajatus.model.AdvertismentTypes;

@Service
public class AdvertismentTypesServiceImpl implements AdvertismentTypesService {
	
	private AdvertismentTypesDAO advertismentTypesDAO;

	public void setAdvertismentTypesDAO(AdvertismentTypesDAO advertismentTypesDAO) {
		this.advertismentTypesDAO = advertismentTypesDAO;
	}
	
	@Override
	@Transactional
	public void addAdvertismentType(AdvertismentTypes advertismentTypes){
		this.advertismentTypesDAO.addAdvertismentType(advertismentTypes);

	}
	
	@Override
	@Transactional
	public void updateAdvertismentType(AdvertismentTypes advertismentTypes){
		this.advertismentTypesDAO.updateAdvertismentType(advertismentTypes); 

	}
	
	@Override
	@Transactional
	public void removeAdvertismentType(int id){
		this.advertismentTypesDAO.removeAdvertismentType(id); 

	}
	
	@Override
	@Transactional
	public List<AdvertismentTypes> listAdvertismentType(){
		
		return this.advertismentTypesDAO.listAdvertismentType();
		
	}
	
	@Override
	@Transactional
	public AdvertismentTypes listAdvertismentTypeById(int id){
		return this.advertismentTypesDAO.listAdvertismentTypeById(id);
	}
	
	


}
