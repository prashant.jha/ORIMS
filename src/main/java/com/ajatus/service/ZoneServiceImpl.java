package com.ajatus.service;

import java.util.ArrayList;
import java.util.List;

  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.ZoneDAO;
import com.ajatus.model.Zone;

@Service
public class  ZoneServiceImpl implements ZoneService {

	@Autowired(required=true)
	private ZoneDAO zDao;
	
	@Override
	@Transactional
	public void addZone(Zone z) {
		zDao.addZone(z);
		
	}

	@Override
	@Transactional
	public void updateZone(Zone z) {
		zDao.updateZone(z);
		
	}

	@Override
	@Transactional
	public List<Zone> listZones() {
		List<Zone> list = new ArrayList<Zone>();
		try {
			list = zDao.listZones();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	}

	@Override
	@Transactional
	public Zone getZoneById(int id) {
		Zone zone = null;
		try {
			zone = zDao.getZoneById(id);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return zone;
	}

	@Override
	@Transactional
	public void removeZone(int id) {
		zDao.removeZone(id);
		
	}

	@Override
	@Transactional
	public List<Zone> listZonesByUlb(int ulb_id) {
		List<Zone> list = new ArrayList<Zone>();
		try {
			list = zDao.listZonesByUlb(ulb_id);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return list;
	}
	

}
