package com.ajatus.service;

import java.util.List;

import com.ajatus.model.UserType;

public interface UserTypeService {

	public void addUserType(UserType p);
	public void updateUserType(UserType p);
	public List<UserType> listUserTypes();
	public UserType getUserTypeById(int id);
	public void removeUserType(int id);
}
