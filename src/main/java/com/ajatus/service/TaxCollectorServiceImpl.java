package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.TaxCollectorDAO;
import com.ajatus.model.TaxCollectorModel;

@Service
public class TaxCollectorServiceImpl implements TaxCollectorService {

	private TaxCollectorDAO taxCollectorDAO;

	public void setTaxCollectorDAO(TaxCollectorDAO taxCollectorDAO) {
		this.taxCollectorDAO = taxCollectorDAO;
	}
	
	@Override
	@Transactional
	public void addTaxCollector(TaxCollectorModel tc) {
		this.taxCollectorDAO.addTaxCollector(tc);
		
	}

	@Override
	@Transactional
	public void updateTaxCollector(TaxCollectorModel tc) {
		this.taxCollectorDAO.updateTaxCollector(tc);
		
	}

	@Override
	@Transactional
	public List<TaxCollectorModel> listTaxCollectors() {
		return this.taxCollectorDAO.listTaxCollectors();
	}

	@Override
	@Transactional
	public TaxCollectorModel getTaxCollectorById(int id) {
		return this.taxCollectorDAO.getTaxCollectorById(id);
	}

	@Override
	@Transactional
	public void removeTaxCollector(int id) {
		this.taxCollectorDAO.removeTaxCollector(id);
	}

	@Override
	@Transactional
	public List<TaxCollectorModel> lisTaxCollectorsByWard(int ward_id) {
		return this.taxCollectorDAO.listTaxCollectorsByWard(ward_id);
	}

}
