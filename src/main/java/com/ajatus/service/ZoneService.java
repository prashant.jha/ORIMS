package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ajatus.model.Zone;


public interface ZoneService {
	
	public void addZone(Zone z);
	public void updateZone(Zone z);
	public List<Zone> listZones();
	public Zone getZoneById(int id);
	public void removeZone(int id);
	public List<Zone> listZonesByUlb(int ulb_id);
}
