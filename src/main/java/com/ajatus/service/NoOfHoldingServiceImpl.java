package com.ajatus.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.NoOfHoldingDAO;
import com.ajatus.model.NoOfHolding;
@Service
public class NoOfHoldingServiceImpl implements NoOfHoldingService {

	@Autowired(required=true)
	private NoOfHoldingDAO noOfHoldingDAO;
	
	public void setNoOfHoldingDAO(NoOfHoldingDAO noOfHoldingDAO){
		this.noOfHoldingDAO=noOfHoldingDAO;
	}
	
	@Override
	@Transactional
	public void addNoOfHolding(NoOfHolding noOfHolding) {
		this.noOfHoldingDAO.addNoOfHolding(noOfHolding);
		
	}

	@Override
	public void updateNoOfHolding(NoOfHolding noOfHolding) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeNoOfHolding(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<NoOfHolding> listNoOfHolding() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NoOfHolding listNoOfHoldingById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
