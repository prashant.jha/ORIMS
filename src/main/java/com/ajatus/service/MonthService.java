package com.ajatus.service;

import java.util.List;

import com.ajatus.model.Month;

public interface MonthService {

	public void addMonth(Month m);
	public void updateMonth(Month m);
	public List<Month> listMonths();
	public Month getMonthById(int id);
	public void removeMonth(int id);
}
