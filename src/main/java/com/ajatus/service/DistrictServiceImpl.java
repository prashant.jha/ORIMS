package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.DistrictDAO;
import com.ajatus.model.District;

@Service
public class DistrictServiceImpl implements DistrictService {

	private DistrictDAO DistrictDAO;
	
	public void setDistrictDAO(DistrictDAO DistrictDAO) {
		this.DistrictDAO = DistrictDAO;
	}
	
	@Override
	@Transactional
	public void addDistrict(District d) {
		this.DistrictDAO.addDistrict(d);
	}

	@Override
	@Transactional
	public void updateDistrict(District d) {
		this.DistrictDAO.updateDistrict(d);
	}

	@Override
	@Transactional
	public List<District> listDistricts() {
		return this.DistrictDAO.listDistricts();
	}

	@Override
	@Transactional
	public District getDistrictById(int id) {
		return this.DistrictDAO.getDistrictById(id);
	}

	@Override
	@Transactional
	public void removeDistrict(int id) {
		this.DistrictDAO.removeDistrict(id);
		
	}

}
