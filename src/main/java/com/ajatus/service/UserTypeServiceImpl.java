package com.ajatus.service;


import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.UserTypeDAO;
import com.ajatus.model.UserType;

@Service
public class UserTypeServiceImpl implements UserTypeService {
	
	private UserTypeDAO UserTypeDAO;

	public void setUserTypeDAO(UserTypeDAO UserTypeDAO) {
		this.UserTypeDAO = UserTypeDAO;
	}

	@Override
	@Transactional
	public void addUserType(UserType p) {
		this.UserTypeDAO.addUserType(p);
	}

	@Override
	@Transactional
	public void updateUserType(UserType p) {
		this.UserTypeDAO.updateUserType(p);
	}

	@Override
	@Transactional
	public List<UserType> listUserTypes() {
		return this.UserTypeDAO.listUserTypes();
	}

	@Override
	@Transactional
	public UserType getUserTypeById(int id) {
		return this.UserTypeDAO.getUserTypeById(id);
	}

	@Override
	@Transactional
	public void removeUserType(int id) {
		this.UserTypeDAO.removeUserType(id);
	}

}
