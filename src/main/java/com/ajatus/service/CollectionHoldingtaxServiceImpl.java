package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.CollectionHoldingTaxDAO;
import com.ajatus.model.CollectionHoldingTax;

@Service
public class CollectionHoldingtaxServiceImpl implements CollectionHoldingTaxService {

	private CollectionHoldingTaxDAO collectionHoldingTaxDAO;
	
	public void setCollectionHoldingTaxDAO(CollectionHoldingTaxDAO collectionHoldingTaxDAO) {
		this.collectionHoldingTaxDAO = collectionHoldingTaxDAO;
	}
	
	@Override
	@Transactional
	public void addCollectionHoldingTax(CollectionHoldingTax h) {
		this.collectionHoldingTaxDAO.addCollectionHoldingTax(h);
		
	}

	@Override
	@Transactional
	public void updateCollectionHoldingTax(CollectionHoldingTax h) {
		this.collectionHoldingTaxDAO.updateCollectionHoldingTax(h);
	}

	@Override
	@Transactional
	public List<CollectionHoldingTax> listCollectionHoldingTaxes() {
		return this.collectionHoldingTaxDAO.listCollectionHoldingTaxes();
	}

	@Override
	@Transactional
	public CollectionHoldingTax getCollectionHoldingTaxById(int id) {
		return this.collectionHoldingTaxDAO.getCollectionHoldingTaxById(id);
	}

	@Override
	@Transactional
	public void removeCollectionHoldingTax(int id) {
		this.collectionHoldingTaxDAO.removeCollectionHoldingTax(id);
		
	}

	@Override
	public List<CollectionHoldingTax> listCollectionHoldingTaxesByWard(int ward_id) {
		// TODO Auto-generated method stub
		return this.collectionHoldingTaxDAO.listCollectionHoldingTaxesByWard(ward_id);
	}

}
