package com.ajatus.service;

import java.util.List;

import com.ajatus.model.CollectionAdvertismentTaxes;

public interface CollectionAdvertismentTaxesService {
	
	public void addCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat);
	public void updateCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat);
	public void removeCollectionAdvertismentTaxes(int id);
	public List<CollectionAdvertismentTaxes> listCollectionAdvertismentTaxes();
	public CollectionAdvertismentTaxes listCollectionAdvertismentTaxesById(int id);


}
