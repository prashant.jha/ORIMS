package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.WardOfficeDAO;
import com.ajatus.model.WardOffice;

@Service
public class WardOfficeServiceImpl implements WardOfficeService{

	private WardOfficeDAO wardOfficeDAO;

	public void setWardOfficeDAO(WardOfficeDAO wardOfficeDAO) {
		this.wardOfficeDAO = wardOfficeDAO;
	}
	
	@Override
	@Transactional
	public void addWardOffice(WardOffice wo) {
		this.wardOfficeDAO.addWardOffice(wo);
	}

	@Override
	@Transactional
	public void updateWardOffice(WardOffice wo) {
		this.wardOfficeDAO.updateWardOffice(wo);
	}

	@Override
	@Transactional
	public List<WardOffice> listWardOffices() {
		return this.wardOfficeDAO.listWardOffices();
	}

	@Override
	@Transactional
	public WardOffice getWardOfficeById(int id) {
		return this.wardOfficeDAO.getWardOfficeById(id);
	}

	@Override
	@Transactional
	public void removeWardOffice(int id) {
		this.wardOfficeDAO.removeWardOffice(id);
	}

	@Override
	@Transactional
	public List<WardOffice> listWardOfficesByWard(int ward_id) {
		return this.wardOfficeDAO.listWardOfficesByWard(ward_id);
	}

}
