package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.CollectionAdvertismentTaxesDAO;
import com.ajatus.model.CollectionAdvertismentTaxes;

@Service
public class CollectionAdvertismentTaxesServiceImpl implements CollectionAdvertismentTaxesService  {

	private CollectionAdvertismentTaxesDAO collectionAdvertismentTaxesDAO;

	public void setCollectionAdvertismentTaxesDAO(CollectionAdvertismentTaxesDAO collectionAdvertismentTaxesDAO) {
		this.collectionAdvertismentTaxesDAO = collectionAdvertismentTaxesDAO;
	}
	
	@Override
	@Transactional
	public void addCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat){
		this.collectionAdvertismentTaxesDAO.addCollectionAdvertismentTaxes(cat);
	}
	
	@Override
	@Transactional
	public void updateCollectionAdvertismentTaxes(CollectionAdvertismentTaxes cat){
		this.collectionAdvertismentTaxesDAO.updateCollectionAdvertismentTaxes(cat);
	}
	
	@Override
	@Transactional
	public void removeCollectionAdvertismentTaxes(int id){
		this.collectionAdvertismentTaxesDAO.removeCollectionAdvertismentTaxes(id);
	}
	
	@Override
	@Transactional
	public List<CollectionAdvertismentTaxes> listCollectionAdvertismentTaxes(){
		return this.collectionAdvertismentTaxesDAO.listCollectionAdvertismentTaxes();
	}
	
	@Override
	@Transactional
	public CollectionAdvertismentTaxes listCollectionAdvertismentTaxesById(int id){
		return this.collectionAdvertismentTaxesDAO.listCollectionAdvertismentTaxesById(id);
	}
	
	
}
