package com.ajatus.service;

import java.util.List;

import com.ajatus.model.DemandTaxCollector;
import com.ajatus.model.DemandWard;

public interface DemandTaxCollectorService {

	public void addDemandTaxCollector(DemandTaxCollector dt);
	public void updateTaxCollector(DemandTaxCollector dt);
	public List<DemandTaxCollector> listDemandTaxCollectors();
	public DemandTaxCollector getDemandTaxCollectorById(int id);
	public List<DemandTaxCollector> listDemandTaxCollectorsByDemandWard(DemandWard demandWard);
	public void removeDemandTaxCollector(int id);
}
