package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.DemandTaxCollectorDAO;
import com.ajatus.model.DemandTaxCollector;
import com.ajatus.model.DemandWard;

@Service
public class DemandTaxCollectorServiceImpl implements DemandTaxCollectorService{

	private DemandTaxCollectorDAO demandTaxCollectorDAO;
	
	
	public void setDemandTaxCollectorDAO(DemandTaxCollectorDAO demandTaxCollectorDAO) {
		this.demandTaxCollectorDAO = demandTaxCollectorDAO;
	}

	@Override
	@Transactional
	public void addDemandTaxCollector(DemandTaxCollector dt) {
		this.demandTaxCollectorDAO.addDemandTaxCollector(dt);
		
	}

	@Override
	@Transactional
	public void updateTaxCollector(DemandTaxCollector dt) {
		this.demandTaxCollectorDAO.updateTaxCollector(dt);
		
	}

	@Override
	@Transactional
	public List<DemandTaxCollector> listDemandTaxCollectors() {
		return this.demandTaxCollectorDAO.listDemandTaxCollectors();
	}

	@Override
	@Transactional
	public DemandTaxCollector getDemandTaxCollectorById(int id) {
		return this.demandTaxCollectorDAO.getDemandTaxCollectorById(id);
	}

	@Override
	@Transactional
	public List<DemandTaxCollector> listDemandTaxCollectorsByDemandWard(DemandWard demandWard) {
		return this.demandTaxCollectorDAO.listDemandTaxCollectorsByDemandWard(demandWard);
	}

	@Override
	@Transactional
	public void removeDemandTaxCollector(int id) {
		this.demandTaxCollectorDAO.removeDemandTaxCollector(id);
		
	}

}
