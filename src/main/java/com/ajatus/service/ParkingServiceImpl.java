package com.ajatus.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.ParkingDAO;
import com.ajatus.model.AssetParkingLotAuctioned;
import com.ajatus.model.AssetParkingLotULB;
import com.ajatus.model.ParkingLots;
import com.ajatus.model.ParkingLotsPast;
import com.ajatus.model.Zone;

@Service
public class ParkingServiceImpl implements ParkingService {

	@Autowired(required=true)
	private ParkingDAO pDao;
	
	@Override
	@Transactional
	public void addParkingLots(ParkingLots parkingLots) {
		pDao.addParkingLots(parkingLots);
		
	}
  
	@Override
	@Transactional
	public void addParkingLotsPast(ParkingLotsPast parkingLotsPast) {
		pDao.addParkingLotsPast(parkingLotsPast);
		
	}

	@Override
	@Transactional
	public void addAssetParkingLotULB(AssetParkingLotULB asParkingLotULB) {
		pDao.addAssetParkingLotULB(asParkingLotULB);
		
	}

	@Override
	@Transactional
	public void addAssetParkingLotAuctioned(AssetParkingLotAuctioned assetParkingLotAuctioned) {
		pDao.addAssetParkingLotAuctioned(assetParkingLotAuctioned);
		
	}
}
