package com.ajatus.service;


import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ajatus.dao.TradeLicenceDAO;
import com.ajatus.model.TradeBigDefaultersModel;
import com.ajatus.model.TradeLicence;
import com.ajatus.model.TradeLicenceCollectionModel;
import com.ajatus.model.TradeLicenceCollectionPastModel;
import com.ajatus.model.TradeLicenceDemandModel;

@Service
public class TradeLicenceServiceImpl implements TradeLicenceService {

	@Autowired(required=true)
	private TradeLicenceDAO tradeLicenceDAO ;

	@Override
	@Transactional 
	public void addTradeLicenceDemand(TradeLicenceDemandModel tradeLicenceDemandModel) {
		tradeLicenceDAO.addTradeLicenceDemand(tradeLicenceDemandModel);  
		
	}

	@Override
	@Transactional 
	public void addTradeLicenceCollection(TradeLicenceCollectionModel tradeLicenceCollectionModel) {
		tradeLicenceDAO.addTradeLicenceDemand(tradeLicenceCollectionModel);  
		
	}

	@Override
	@Transactional 
	public void addTradeLicenceCollectionPast(TradeLicenceCollectionPastModel tradeLicenceCollectionPastModel) {
		tradeLicenceDAO.addTradeLicenceCollectionPast(tradeLicenceCollectionPastModel);  
		
	}

	@Override
	@Transactional 
	public void addNoTradeLicence(TradeLicence tLicence) {
        tradeLicenceDAO.addNoTradeLicence(tLicence);  
	}

	@Override
	@Transactional 
	public void addTradeBigDefaulters(TradeBigDefaultersModel tradeBigDefaultersModel) {
		  tradeLicenceDAO.addTradeBigDefaulters(tradeBigDefaultersModel);  
			
	}
	
}
