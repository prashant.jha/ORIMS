package com.ajatus.service;

import com.ajatus.model.AssetParkingLotAuctioned;
import com.ajatus.model.AssetParkingLotULB;
import com.ajatus.model.ParkingLots;
import com.ajatus.model.ParkingLotsPast;

public interface ParkingService {
	
	public void addParkingLots(ParkingLots  parkingLots) ;

	public void addParkingLotsPast(ParkingLotsPast parkingLotsPast);

	public void addAssetParkingLotULB(AssetParkingLotULB asParkingLotULB);

	public void addAssetParkingLotAuctioned(AssetParkingLotAuctioned assetParkingLotAuctioned);

}
