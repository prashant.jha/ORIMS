package com.ajatus.service;

import java.util.List;

import com.ajatus.model.CollectionHoldingTax;

public interface CollectionHoldingTaxService {

	public void addCollectionHoldingTax(CollectionHoldingTax h);
	public void updateCollectionHoldingTax(CollectionHoldingTax h);
	public List<CollectionHoldingTax> listCollectionHoldingTaxes();
	public List<CollectionHoldingTax> listCollectionHoldingTaxesByWard(int ward_id);
	public CollectionHoldingTax getCollectionHoldingTaxById(int id);
	public void removeCollectionHoldingTax(int id);
}
