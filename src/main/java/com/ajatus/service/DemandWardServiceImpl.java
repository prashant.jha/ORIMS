package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.DemandWardDAO;
import com.ajatus.model.DemandWard;

@Service
public class DemandWardServiceImpl implements DemandWardService{

	private DemandWardDAO demandWardDAO;
	
	public void setDemandWardDAO(DemandWardDAO demandWardDAO) {
		this.demandWardDAO = demandWardDAO;
	}
	
	@Override
	@Transactional
	public void addDemandWard(DemandWard dw) {
		this.demandWardDAO.addDemandWard(dw);
	}

	@Override
	@Transactional
	public void updateDemandWard(DemandWard dw) {
		this.demandWardDAO.updateDemandWard(dw);
	}

	@Override
	@Transactional
	public List<DemandWard> listDemandWards() {
		return this.demandWardDAO.listDemandWards();
	}

	@Override
	@Transactional
	public DemandWard getDemandWardById(int id) {
		return this.demandWardDAO.getDemandWardById(id);
	}

	@Override
	@Transactional
	public void removeDemandWard(int id) {
		this.demandWardDAO.removeDemandWard(id);
		
	}
	@Override
	@Transactional
	public boolean validateWardFinancialYear(int ward,int financialYear){
		
	 return	this.demandWardDAO.validateWardFinancialYear(ward, financialYear);
	}

	

}
