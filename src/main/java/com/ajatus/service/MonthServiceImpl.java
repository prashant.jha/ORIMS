package com.ajatus.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.MonthDAO;
import com.ajatus.model.Month;

@Service
public class MonthServiceImpl implements MonthService{

	private MonthDAO monthDAO;
	
	public void setMonthDAO(MonthDAO monthDAO) {
		this.monthDAO = monthDAO;
	}
	
	@Override
	@Transactional
	public void addMonth(Month m) {
		this.monthDAO.addMonth(m);
		
	}

	@Override
	@Transactional
	public void updateMonth(Month m) {
		this.monthDAO.updateMonth(m);
		
	}

	@Override
	@Transactional
	public List<Month> listMonths() {
		return this.monthDAO.listMonths();
	}

	@Override
	@Transactional
	public Month getMonthById(int id) {
		return this.monthDAO.getMonthById(id);
	}

	@Override
	@Transactional
	public void removeMonth(int id) {
		this.monthDAO.removeMonth(id);
	}

}
