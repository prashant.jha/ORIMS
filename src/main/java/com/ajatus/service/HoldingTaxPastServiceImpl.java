package com.ajatus.service;

import java.util.List;

import org.hibernate.type.TrueFalseType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajatus.dao.HoldingTaxPastDAO;
import com.ajatus.model.HoldingTaxPast;

@Service
public class HoldingTaxPastServiceImpl implements HoldingTaxPastService {

	@Autowired(required=true)
	private HoldingTaxPastDAO holdingTaxPastDAO;
	
	public void setHoldingTaxPastDAO(HoldingTaxPastDAO holdingTaxPastDAO){
		this.holdingTaxPastDAO=holdingTaxPastDAO;
	}
	
	@Override
	@Transactional
	public void addHoldingTaxPast(HoldingTaxPast holdingTaxPast) {
		this.holdingTaxPastDAO.addHoldingTaxPast(holdingTaxPast);
		
	}

	@Override
	public void updateHoldingTaxPast(HoldingTaxPast holdingTaxPast) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeHoldingTaxPast(int id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<HoldingTaxPast> listHoldingTaxPast() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HoldingTaxPast listHoldingTaxPastById(int id) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
