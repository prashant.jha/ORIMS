package com.ajatus.service;
import com.ajatus.model.User;
import com.ajatus.model.UserType;
import com.ajatus.jpa.UserJPA;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.HashSet;
import java.util.Set;
@Service
public class UserDetailServiceImpl implements UserDetailsService{
	@Autowired
	private UserJPA userJPA;
	@Override
	@Transactional(readOnly = true)
	
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		
		
		 User user = userJPA.findByUserName(userName);
		 
		 System.out.println("user type   ====   "+user.getUserType());
		 UserType userType = user.getUserType();
	        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
	       
	            grantedAuthorities.add(new SimpleGrantedAuthority(userType.getExecutingAuthority()));

	        return new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPasswordHash(), grantedAuthorities);
	    }
	}
	
  

