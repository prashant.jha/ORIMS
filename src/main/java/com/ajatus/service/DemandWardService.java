package com.ajatus.service;

import java.util.List;

import com.ajatus.model.DemandWard;

public interface DemandWardService {

	public void addDemandWard(DemandWard dw);
	public void updateDemandWard(DemandWard dw);
	public List<DemandWard> listDemandWards();
	public DemandWard getDemandWardById(int id);
	public void removeDemandWard(int id);
	public boolean validateWardFinancialYear(int ward,int financialYear);
	
}
