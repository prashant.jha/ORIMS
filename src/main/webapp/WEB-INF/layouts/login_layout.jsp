<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="../layouts/taglib.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<title><tiles:getAsString name="title" /></title>
		
		<meta
			content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
			name="viewport">
		
		<tilesx:useAttribute id="stylesheets" name="stylesheets"
			classname="java.util.List" />
		<tilesx:useAttribute id="javascripts" name="javascripts"
			classname="java.util.List" />
		
		<!-- Font Awesome -->
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		<!-- Ionicons -->
		<link rel="stylesheet"
			href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<style>
			.login-logo a, .register-logo a {
    			font-weight: bold;
    			color:#3c8dbc !important;
			}
		</style>
		<!-- 	All CSS Files -->
		<c:forEach var="css" items="${stylesheets}">
			<link rel="stylesheet" type="text/css" href="<c:url value="${css}"/>">
		</c:forEach>
		
		<!-- 	All JS Files -->
		<c:forEach var="script" items="${javascripts}">
			<script src="<c:url value="${script}"/>"></script>
		</c:forEach>
		
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		  <![endif]-->
	</head>
	
	<body class="hold-transition login-page">
	<style>
	  .login-box{
	  	width:100%;
	  	background:transparent !important;
	  }
	  .login-box-body{
	  	background:transparent !important;
	  }
	  .login-logo{
	  	margin-left:200px;
	  	width:900px;
	  }
	</style>
<div class="login-box">
  <div class="login-logo">
  	
   <h1 style="font-size:50px;color:#fff">ORIMS</h1> 
  <a href="#" style="font-size:22px;color:#fff !important">Odisha Revenue Information Management System for ULBs</a>
  </div>
  <!-- /.login-logo -->
  
  <!-- Main content -->
			<tiles:insertAttribute name="content" />
  
</div>
<!-- /.login-box -->
<footer class="main-footer" style="background:none;">
	
		<div class="pull-right hidden-xs" style="color:#fff;margin-top:60px;margin-right:100px">
			<b>Powered by <u>Deloitte Touche Tohmatsu India LLP.</u>
		</div>
		
	
	<!-- /.container -->
</footer>
<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
	
