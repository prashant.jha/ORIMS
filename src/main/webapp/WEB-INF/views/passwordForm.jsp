<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="min-height: 40px;">
        
      </section>

      <!-- Main content -->
      <section class="content" style="margin-left: 200px;margin-right: 200px;">

		<form:form action="changePassword" class="form-changepasword" commandName="Project" cssClass="form-horizontal">
      	<div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><strong><u>Change Your Password</u></strong></h3>
            <!-- /.box-header -->
            <div class="box-body">
				<div class="row">					    
					<div class="form-group ${errorOldPassword != null ? 'has-error' : ''}">
					    <label for="oldpassword" class="control-label col-sm-3">Old Password:</label>
					    <div class="col-sm-9">
					      <input name="oldPassword" type="password" class="form-control" placeholder="Old Password"
                   autofocus="true"/>
                   			
                   			<span class="help-block">${errorOldPassword}</span>
					    </div>
					    
					</div>
					<div class="form-group has-feedback">
					    <label for="newPassword" class="control-label col-sm-3">New Password:</label>
					    <div class="col-sm-9">
					      <input name="newPassword" type="password" class="form-control" placeholder="New Password"
                   autofocus="true"/>
					    </div>
					</div>
					<div class="form-group ${errorConfrimPassword != null ? 'has-error' : ''}">
					    <label for="confirmPassword" class="control-label col-sm-3">Confirm Password:</label>
					    <div class="col-sm-9">
					      <input name="confirmPassword" type="password" class="form-control" placeholder="confirm Password"
                   autofocus="true"/>
                   			<span class="help-block">${errorConfrimPassword}</span>
					    </div>
					    
					    
					</div>
					<div class="form-group has-success">
						<span class="help-block" style="margin-left:100px">${success}</span>
					</div>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
			        
			        <div class="col-sm-9 col-sm-offset-9">
			            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-check"></i>&nbsp;&nbsp;Change Password</button>
			        </div>
			    </div>
            </div>
            <!-- /.box-body -->
          </div>
          </form:form>	
      </section>
   </div>
 </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
