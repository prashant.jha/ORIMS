$(function () {
    Highcharts.chart('growth-chart', {
        chart: {
            zoomType: 'xy'
        },
            title:{
            text:''
        },
        subtitle:{
            text:'Total Projects:77, Total value: 108 Cr <br />Pending at stages'
        },
        colors:['#CFEBA9','#0098A7'],
        
        
        exporting: 
        { 
            enabled: false 
        },
        
        plotOptions: {
                series: {
                
                }
            },
        xAxis: [{
            categories: ['DPR','Tech. Sanc.','A/A','DTCN','Tender','LOI','Contract','Site Mob.'],
            crosshair: true
        }],
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}'
            },
            title: {
                text: 'Value In Crores',
                style: {
                    color: '#000000'
                }
            },
            opposite: true

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'No Of Projects '
            },
            labels: {
                format: '{value}'
            }

        }],
        tooltip: {
            shared: true
        },
//        legend: {
//            layout: 'vertical',
//            align: 'left',
//            x: 80,
//            verticalAlign: 'top',
//            y: 55,
//            floating: true,
//            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#000000'
//        },
        series: [{
            name: 'No Of Projects',
            type: 'column',
            yAxis: 1,
            data: [10, 12, 6, 8, 14, 7, 9, 11],
            tooltip: {
                valueSuffix: ''
            }
            

        }, {
            name: 'Value In Crores',
            type: 'line',
            data: [17, 18, 12, 10, 20, 12, 9, 10],
            tooltip: {
                valueSuffix: ' Cr'
            },
            dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: '#000',
                        textShadow: 'false',
                        textOutline: false
                        }
            }
        }
           ]
    });
});
var DataToolTip1= '<table><tr><td><b>Project Name:</b></td><td> Bridge Contruction</td></tr><tr><td><b>Contractor Name:</b></td><td>Atish Sahoo</td></tr><tr><td><b>Delay Reason:</b></td><td> Some Reason</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><b>Project Name:</b></td><td> Bridge Contruction</td></tr><tr><td><b>Contractor Name:</b></td><td>Atish Sahoo</td></tr><tr><td><b>Delay Reason:</b></td><td> Some Reason</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td><b>Project Name:</b></td><td> Bridge Contruction</td></tr><tr><td><b>Contractor Name:</b></td><td>Atish Sahoo</td></tr><tr><td><b>Delay Reason:</b></td><td> Some Reason</td></tr></table>';
 
$(function () {
    Highcharts.chart('Analysis-Of-Targets', {
        chart: {
            type: 'bar'
        },
        title: {
            text: ''
        },
        exporting: 
        { 
            enabled: false 
        },
        colors:['#8397AE','#295237','#00A5DB','#002365','#0098A7','#75787B','#B7E280','#82BB46'],
        xAxis: {
            categories: ['&lt;15 Days', '15-30 Days', '30-60 Days', '60-90 Days', '&gt;90 Days']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No Of Projects'
            }
        },
        legend: {
            reversed: false
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: '#FFF',
                        textShadow: 'false',
                        textOutline: false
                        }
                },
                
            }
        },
        tooltip: {
               useHTML: true,
               formatter: function () {
                   
                    return this.series.xAxis.categories[this.point.x]+'<br /><b>'+this.series.name+'</b>: '+this.point.y +'<br/>'+ this.point.myData + '';
                }
            },
        series: [{
            name: 'DPR',
            data: [
                {y: 5,myData: DataToolTip1},
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 7,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1}
            ]
        }, {
            name: 'Tech. Sanc.',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1},
                {y: 3,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        }, {
            name: 'A/A',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        },
        {
            name: 'DTCN',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        },
        {
            name: 'Tender',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        },
        {
            name: 'LOI',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        },
        {
            name: 'Contract',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        },
        {
            name: 'Site Mob.',
            data: [
                {y: 3,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 4,myData: DataToolTip1},
                {y: 2,myData: DataToolTip1},
                {y: 5,myData: DataToolTip1}
            ]
        }]
    });
});
  
function ShowColorBasedOnData(y,total)
        {
           if(y!=0)
               per = parseInt(y)/parseInt(total) * 100;
            
               if (per<10)
                return '#50B432';
                else if(per<=30)
                return '#DDDF00';
                else if(per<=50)
                return '#FFFF22';
                else if(per<=70)
                return  '#ffb61B';
                else 
                return '#FF0011';   
                
            
            
        }
            
            



        
        
$(function () {
    
    
    Highcharts.chart('average-time-analysis', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['DPR', 'Tech. Sanc.', 'A/A', 'DTCN', 'Tender','LOI','Contract','Site Mob.']
        },
        exporting: 
        { 
            enabled: false 
        },
        colors:['#CFEBA9','#0098A7'],
        yAxis: {
            min: 0,
            title: {
                text: 'No Of Days'
            }
        },
        tooltip: {
             valueSuffix: ' Days'
//             formatter:function(){
//                var txt = '',
//                    x = this.x,
//                    series = this.point.series.chart.series;
//                    txt += '<b>'+x+'</b><br />'
//                    var total = 0;
//                    $.each(series,function(i,s){
//                      if (s.name=='Min'){
//                      $.each(s.data,function(j, d){
//                                    name = this.series.xAxis.categories[d.x];
//                                    if(name ===x){
//                                        
//                                    txt += 'Best:'+d.y+' Days<br />';
//                                    }
//                                   
//                            });
//                      }
//                          
//                          $.each(s.data,function(j, d){
//                                    name = this.series.xAxis.categories[d.x];
//                                    if(name ===x){
//                                    total += d.y;  
//                                    
//                                    }
//                                   
//                            });
//                        
//                      
//                          
//                    });
//                     txt += 'Average:'+total+ ' Days';
//                      return txt;
//                    }
        },
        plotOptions: {
            column: {
                
                dataLabels: {
                    enabled: true,

                }
            }
        },
        series: [
            {
            name: 'Average',
            data: [28,30,40,35,42,29,33,40]
            },
            {
            name: 'Best',
            data: [8,15,20,17,15,17,15,20]
        }, ]
    });
});        
    
$(function () {
    
    
    Highcharts.chart('physical-financial', {
        chart: {
            type: 'column',
           
        },
        title: {
            text: ''
        },
        legend:false,
        xAxis: {
            categories: ['Physical', 'Financial']
        },
        exporting: 
        { 
            enabled: false 
        },
        colors:['#CFEBA9','#0098A7','#00A5DB','#295237'],
        yAxis: {
            min: 0,
            title: {
                text: 'No Of Projects'
            }
        },
        
        tooltip: {
//             formatter:function(){
//                var txt = '',
//                    x = this.x,
//                    series = this.point.series.chart.series;
//                    txt += '<b>'+x+'</b><br />'
//                    var total = 0;
//                    $.each(series,function(i,s){
//                      if (s.name=='Min'){
//                      $.each(s.data,function(j, d){
//                                    name = this.series.xAxis.categories[d.x];
//                                    if(name ===x){
//                                        
//                                    txt += 'Best:'+d.y+' Days<br />';
//                                    }
//                                   
//                            });
//                      }
//                          
//                          $.each(s.data,function(j, d){
//                                    name = this.series.xAxis.categories[d.x];
//                                    if(name ===x){
//                                    total += d.y;  
//                                    
//                                    }
//                                   
//                            });
//                        
//                      
//                          
//                    });
//                     txt += 'Average:'+total+ ' Days';
//                      return txt;
//                    }
        },
        plotOptions: {
            column: {
                stacking:'normal',
                dataLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: '#000',
                        textShadow: 'false',
                        textOutline: false
                        }

                }
            }
            
        },
        series: [
            {
            name: '<25 %',
            data: [28,30]
            },
            {
            name: '25 - >50%',
            data: [8,15]
            },
            {
            name: '50 - >75%',
            data: [8,15]
            },
            {
            name: '75 - 100%',
            data: [8,15]
            },
        
        ]
    });
});




$(function () {
    Highcharts.chart('time-analysis', {
        chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
             exporting: 
            { 
            enabled: false 
            },
             colors:['#E56969','#82bb46'],
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br /> project Value: <b>{point.y}</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    style: {
                        fontWeight: 'bold',
                        color: '#000',
                        textShadow: 'false',
                        textOutline: false
                        },
                    distance: -25,
                    color:'white'
                }
            },
                pie: {
                    size: 90,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: [ {
                    name: 'Delayed',
                    y: 70,
                   // sliced: true,
                    selected: true
                },
                {
                    name: 'On Time',
                    y: 30
                }]
            }]
        });
    }); 
        
$(function () {
    Highcharts.chart('project-analysis', {
        chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            colors:['#82BB46','#75787B','#0098A7','#00A5DB','#295237'],
             exporting: 
            { 
            enabled: false 
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br /> project Value: <b>{point.y}</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage) + ' %';
                    },
                    style: {
                        fontWeight: 'bold',
                        textShadow: 'false',
                        textOutline: false
                        },
                    distance: -25,
                    color:'white'
                }
            },
                pie: {
                    size: 150,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: [{
                    name: '>= 10 Cr',
                    y: 10
                }, {
                    name: '5 Cr - &lt;10 Cr',
                    y: 9,
                   // sliced: true,
                    selected: true
                }, {
                    name: '1 Cr - &lt;5 Cr',
                    y: 11
                }, {
                    name: '25 L - &lt;1 Cr',
                    y: 7
                }, {
                    name: '&lt; 25 L',
                    y: 9
                }]
            }]
        });
    }); 
    
    $(function () {
    Highcharts.chart('project-analysis-type', {
        chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: ''
            },
            colors:['#00A5DB','#D6DCE4','#ACB9C8','#8397AE','#323F4E'],
            legend: {
              width: 200,
              itemWidth: 300
            },
             exporting: 
            { 
            enabled: false 
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br /> project Value: <b>{point.y}</b>'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                series: {
                dataLabels: {
                    enabled: true,
                    formatter: function() {
                        return Math.round(this.percentage*100)/100 + ' %';
                    },
                    style: {
                        fontWeight: 'bold',
                        textShadow: 'false',
                        textOutline: false
                        },
                    distance: -25,
                    color:'white'
                }
            },
                pie: {
                    size: 150,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: [{
                    name: 'Sewerage ',
                    y: 10
                },{
                    name: 'Parks \ \ ',
                    y: 5
                }, 
                {
                    name: 'Roads And Bridges',
                    y: 10,
                   // sliced: true,
                    selected: true
                }, {
                    name: 'Buildings',
                    y: 15
                },  {
                    name: 'Others',
                    y: 10
                }]
            }]
        });
    });
        
//    $(function () {
//    Highcharts.chart('financial-progress', {
//        chart: {
//                plotBackgroundColor: null,
//                plotBorderWidth: null,
//                plotShadow: false,
//                type: 'pie'
//            },
//            title: {
//                text: ''
//            },
//            legend: {
//              width: 200,
//              itemWidth: 300
//            },
//             exporting: 
//            { 
//            enabled: false 
//            },
//            colors:['#046A38','#E30613','#53565A','#0076A8','#FFD500'],
//            tooltip: {
//                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br /> project Value: <b>{point.y}</b>'
//            },
//            credits: {
//                enabled: false
//            },
//            plotOptions: {
//                series: {
//                dataLabels: {
//                    enabled: true,
//                    formatter: function() {
//                        return Math.round(this.percentage*100)/100 + ' %';
//                    },
//                    distance: -25,
//                    color:'white'
//                }
//            },
//                pie: {
//                    size: 150,
//                    allowPointSelect: true,
//                    cursor: 'pointer',
//                    dataLabels: {
//                        enabled: false
//                    },
//                    showInLegend: true
//                }
//            },
//            series: [{
//                name: 'Percentage',
//                colorByPoint: true,
//                data: [{
//                    name: '0-25%',
//                    y: 12
//                }, {
//                    name: '25-50 %',
//                    y: 7,
//                   // sliced: true,
//                    selected: true
//                }, {
//                    name: '50-75 %',
//                    y: 19
//                }, {
//                    name: '75-100 %',
//                    y: 4
//                }, {
//                    name: '>100 %',
//                    y: 4
//                }]
//            }]
//        });
//    });    
        
//        $(function () {
//    Highcharts.chart('physical-progress', {
//        chart: {
//                plotBackgroundColor: null,
//                plotBorderWidth: null,
//                plotShadow: false,
//                type: 'pie'
//            },
//            title: {
//                text: ''
//            },
//            legend: {
//              width: 200,
//              itemWidth: 300
//            },
//             exporting: 
//            { 
//            enabled: false 
//            },
//            colors:['#F39200','#005587','#009A44','#53565A','#C81340'],
//            tooltip: {
//                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b><br /> project Value: <b>{point.y}</b>'
//            },
//            credits: {
//                enabled: false
//            },
//            plotOptions: {
//                series: {
//                dataLabels: {
//                    enabled: true,
//                    formatter: function() {
//                        return Math.round(this.percentage*100)/100 + ' %';
//                    },
//                    distance: -25,
//                    color:'white'
//                }
//            },
//                pie: {
//                    size: 150,
//                    allowPointSelect: true,
//                    cursor: 'pointer',
//                    dataLabels: {
//                        enabled: false
//                    },
//                    showInLegend: true
//                }
//            },
//            series: [{
//                name: 'Percentage',
//                colorByPoint: true,
//                data: [{
//                    name: '0-25%',
//                    y: 10
//                }, {
//                    name: '25-50 %',
//                    y: 9,
//                   // sliced: true,
//                    selected: true
//                }, {
//                    name: '50-75 %',
//                    y: 11
//                }, {
//                    name: '>75 %',
//                    y: 7
//                }]
//            }]
//        });
//    });    

//    $(function () {
//    Highcharts.chart('last-quarter-analysis', {
//        chart: {
//            plotBackgroundColor: null,
//            plotBorderWidth: 0,
//            plotShadow: false
//        },
//        // title: {
//        //     text: '37',
//        //     align: 'center',
//        //     verticalAlign: 'middle',
//        //     y: 40
//        // },
//        title: '',
//        exporting: 
//        { 
//            enabled: false 
//        },
//        tooltip: {
//            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
//        },
//        credits: {
//                enabled: false
//            },
//        colors:['#43B02A','#0097A9','#6FC2B4','#75787B','#E3E48D','#004F59','#D0D0CE'],
//        plotOptions: {
//            pie: {
//                dataLabels: {
//                    enabled: true,
//                    formatter: function() {
//                        return Math.round(this.percentage*100)/100 + ' %';
//                    },
//                    distance: -10,
//                    style: {
//                        fontWeight: 'bold',
//                        color: 'white'
//                    }
//                },
//                size: 150,
//                startAngle: 0,
//                endAngle: 360,
//                center: ['50%', '75%'],
//                showInLegend: true
//            }
//        },
//        series: [{
//            type: 'pie',
//            name: 'Projects',
//            innerSize: '50%',
//            data: [
//                ['DPR',   10],
//                ['Technical Sanction',       20],
//                ['Admin Approval', 25],
//                ['DTCN Approval',    15],
//                ['Tender Approval',     10],
//                ['Agreement Execution',     5],
//                ['Site Mobilization',     15],
//                {
//                    name: 'Proprietary or Undetectable',
//                    y: 0.2,
//                    dataLabels: {
//                        enabled: false
//                    }
//                }
//            ]
//        }]
//    });
//});
/* Root Cause Analysis */ 

$(function () {
    Highcharts.chart('root-cause-DPR', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        credits: {
                enabled: false
            },
        exporting: 
        { 
            enabled: false 
        },
        title: {
            text: 'DPR',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        colors:['#E3E4E5','#82BB46','#C8C9CA','#ACAEB0','#585A5C'],
        series: [{
            type: 'pie',
            name: 'Reasons',
            innerSize: '50%',
            data: [
                ['R1',   10.38],
                ['R2',   56.33],
                ['R3',   24.03],
                ['R4',    4.77],
                ['R4',    0.91]
            ]
        }]
    });
});

$(function () {
    Highcharts.chart('root-cause-Technical', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        credits: {
                enabled: false
            },
        exporting: 
        { 
            enabled: false 
        },
        title: {
            text: 'Tech. Sanc.',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        colors:['#E3E4E5','#C8C9CA','#00A5DB','#ACAEB0','#585A5C'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   10.38],
                ['R2',   10.33],
                ['R3',   24.03],
                ['R4',   20.77],
                ['R5',    9.91]
            ]
        }]
    });
});

$(function () {
    Highcharts.chart('root-cause-AA', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        exporting: 
        { 
            enabled: false 
        },
        title: {
            text: 'A/A',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        credits: {
                enabled: false
            },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },colors:['#E3E4E5','#C8C9CA','#295237','#ACAEB0','#585A5C'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   15.38],
                ['R2',   12.33],
                ['R3',   24.03],
                ['R4',   20.77],
                ['R5',    9.91]
            ]
        }]
    });
});

$(function () {
    Highcharts.chart('root-cause-DTCN', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        exporting: 
        { 
            enabled: false 
        },
        credits: {
                enabled: false
            },
       title: {
            text: 'DTCN',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        colors:['#E3E4E5','#C8C9CA','#ACAEB0','#585A5C','#0098A7'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   15.38],
                ['R2',   15.33],
                ['R3',   25.03],
                ['R4',   15.77],
                ['R5',   19.91]
            ]
        }]
    });
});

$(function () {
    Highcharts.chart('root-cause-Tender', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        exporting: 
        { 
            enabled: false 
        },
       title: {
            text: 'Tender',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        credits: {
                enabled: false
            },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
       colors:['#E3E4E5','#C8C9CA','#20A2CC','#585A5C','#ACAEB0','#3A3C3D'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   15.38],
                ['R2',   15.33],
                ['R3',   25.03],
                ['R4',   15.77],
                ['R5',   19.91],
                ['R3',   22.03]
            ]
        }]
    });
});

$(function () {
    Highcharts.chart('root-cause-LOI', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        exporting: 
        { 
            enabled: false 
        },
        credits: {
                enabled: false
            },
       title: {
            text: 'LOI',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        colors:['#E3E4E5','#C8C9CA','#002365','#585A5C','#ACAEB0'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   15.38],
                ['R2',   15.33],
                ['R3',   25.03],
                ['R4',   15.77],
                ['R5',   19.91]
            ]
        }]
    });
});


$(function () {
    Highcharts.chart('root-cause-Contract', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        exporting: 
        { 
            enabled: false 
        },
        title: {
            text: 'Contract',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        credits: {
                enabled: false
            },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
       colors:['#f15c80','#C8C9CA','#ACAEB0','#585A5C','#E3E4E5'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   15.38],
                ['R2',   12.33],
                ['R3',   12.03],
                ['R4',   12.77],
                ['R5',   10.91]
            ]
        }]
    });
});


$(function () {
    Highcharts.chart('root-cause-Mobilization', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        exporting: 
        { 
            enabled: false 
        },
        credits: {
                enabled: false
            },
        title: {
            text: 'Site Mob.',
            align: 'center',
            verticalAlign: 'middle',
            style:{
                font: 'normal 10px Verdana, sans-serif',
            },
            y:30
            
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                size: 100,
                dataLabels: {
                    enabled: false,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white'
                    }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
            }
        },
        colors:['#E3E4E5','#C8C9CA','#ACAEB0','#585A5C','#0098A7'],
        series: [{
            type: 'pie',
            name: 'Reason',
            innerSize: '50%',
            data: [
                ['R1',   15.38],
                ['R2',   15.33],
                ['R3',   25.03],
                ['R4',   15.77],
                ['R5',   19.91]
            ]
        }]
    });
});

$(function () {

    Highcharts.chart('quarter-movement', {
        chart: {
            type: 'funnel',
            marginRight:100
        },
        title: {
            text: '',
            x: -50
        },
        exporting: 
        { 
            enabled: false 
        },
        credits: {
                enabled: false
        },
        colors:['#CFEBA9','#0098A7','#00A5DB','#295237','#002365','#295237'],
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b> ({point.y:,.0f})',
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black',
                    softConnector: true
                },
                neckWidth: '30%',
                neckHeight: '25%'

                //-- Other available options
                // height: pixels or percent
                // width: pixels or percent
            }
        },
        legend: {
            enabled: false
        },
        series: [{
            name: 'Projects',
            data: [
                ['DPR', 10],
                ['Tech. Sanc.', 7],
                ['A/A', 5],
                ['DTCN', 3],
                ['Tender', 2],
                ['LOI', 1]
                
            ]
        }]
    });
});
