(function ( $ ) {
 
    $.fn.ajcalendar = function( options ) {
 
        // Default Options
        var settings = $.extend({
            // These are the defaults.
            color: "#556b2f",
            headerColor: "#000000",
            headerBgColor:"#DDDDDD",
            backgroundColor: "#000",
            calendarType: "Yearly", // Yearly, Monthly, Daily
            showProjectNos: true,
            data:''
        }, options );
 
        // Greenify the collection based on the settings variable.
        // return this.css({
        //     	color: settings.color,
        //     	backgroundColor: settings.backgroundColor
        // 	});
        var html = '<table class="ajCalendar" style="color:'+settings.color+'"><tr style="background:'+settings.backgroundColor+';color:'+settings.headerColor+'"><th colspan="4" style="background:'+settings.headerBgColor+';color:'+settings.headerColor+';border-radius:5px;text-align:center"> 2017</th></tr>';
        $.each(settings.data, function(val,data){
        	if(data.value=='0' || data.value== '')
        		data.value='';
            var Projects = '';
            if(data.value!='')
                Projects = 'Projects';
            switch(val){
                case 'Jan': html+= '<tr style="background:'+settings.backgroundColor+'" style="background:'+settings.color+'"><td> <span class="mname">Jan</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                case 'Feb': html+= '<td> <span class="mname">Feb</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                case 'Mar': html+= '<td> <span class="mname">Mar</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Apr': html+= '<td> <span class="mname">Apr</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td></tr>';break;
                 case 'May': html+= '<tr style="background:'+settings.backgroundColor+'" style="background:'+settings.color+'"><td> <span class="mname">May</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Jun': html+= '<td> <span class="mname">Jun</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Jul': html+= '<td> <span class="mname">Jul</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Aug': html+= '<td> <span class="mname">Aug</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td></tr>';break;
                 case 'Sep': html+= '<tr style="background:'+settings.backgroundColor+'" style="background:'+settings.color+'"><td> <span class="mname">Sep</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Oct': html+= '<td> <span class="mname">Oct</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Nov': html+= '<td> <span class="mname">Nov</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td>';break;
                 case 'Dec': html+= '<td> <span class="mname">Dec</span><span class="noOfProjects">'+data.value+'</span><span class="count">'+Projects+'</span></td></tr>';break;
            }
        } );
	return this.html(html);
 
    };
 
}( jQuery ));